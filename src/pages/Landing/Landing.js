import React from 'react';
import 'srcstaticimagesCounselLogo.svg';
import CounselLogo from '../../static/images/CounselLogo.svg';
import { Link } from 'react-router-dom';
import GxNvest from '../../static/images/Gx_Nvest.svg';
import './Landing.scss';

function Landing() {
  return (
    <div className="login-ag">
      <div className="wrapper">
        <img src={CounselLogo} alt="no_img" />
        <Link to="/login" className="login-btn-ag">
           Access Counsel
        </Link>
        <Link to="" className="get-srt-btn">
           Get Counsel
        </Link>
      </div>
      <div className="powered-by">
        <h5>P O W E R E D &nbsp;&nbsp; B Y</h5>
        <img src={GxNvest} alt="no_img" />
      </div>
    </div>
  );
}

export default Landing;
