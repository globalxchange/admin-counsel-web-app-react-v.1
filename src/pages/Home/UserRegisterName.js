import React, { useState } from 'react';
import whitearrow from "../../static/images/sidebar-icons/logo/arrow-white.png"
import { Select, message } from 'antd';



function UserRegisterName(props) {
    const [name, setname] = useState("")
    const [number, setnumber] = useState("")
    const [acctype, setacctype] = useState('')
    console.log(props.email[0], props.email[1])
    const { Option } = Select;
    async function onChange(value) {
        console.log(`selected ${value}`);
        await setacctype(`${value}`)
        console.log("the-value " + acctype)
    }

    function onBlur() {
        console.log('blur');
    }

    function onFocus() {
        console.log('focus');
    }

    function onSearch(val) {
        console.log('search:', val);
    }
    const handleChange = async (e) => {
        e.preventDefault()
        await setnumber(e.target.value)
        console.log("number " + number)
    }
    const handleChange1 = (e) => {
        e.preventDefault()
        setname(e.target.value)
        console.log("name " + name)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (acctype === "") {
            message.error("please Choose Account type")
        } else {
            props.PasswordPage()
            props.sendName([name, props.email, acctype, number])
            console.log("thename " + name)
        }
    }
    return <div className="outer">
        <div className="middle">
            <div className="inner">
                <h4 className="usrnm-sty">Enter all the fields</h4>
                <p className="gxtitle-sty">GX Registration</p>
                <form onSubmit={handleSubmit}>
                    <div className="crd-style">
                        <p className="adjust-title">Choose Account Type</p>
                        <div className="drop-sty">
                            <Select
                                showSearch
                                style={{ width: 512 }}
                                placeholder="Select Account type"
                                optionFilterProp="children"
                                bordered={false}
                                onChange={onChange}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                defaultActiveFirstOption={true}
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                <Option value="personal">Personal</Option>
                                <Option value="business">Business</Option>
                                <Option value="broker">Broker</Option>
                                <Option value="institutional">Institutional</Option>
                            </Select>
                        </div>
                    </div>
                    <div className="crd-style">
                        <p className="adjust-title">Affiliate Id</p>
                        <input type="number" name="number" className="thetext-styy1" value={number} placeholder="Ex: 1" onChange={handleChange} required />
                    </div>
                    <div className="crd-style">
                        <p className="adjust-title">Assign username</p>
                        <input type="text" name="username" className="thetext-styy1" value={name} placeholder="Ex: Shorupan" onChange={handleChange1} required />
                    </div>
                    <div>
                        <img src={whitearrow} alt="backarrow" className="whitearrow-sty" onClick={() => props.onClose()} />
                        <button type="submit" className="centerbtn-sty">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
}

export default UserRegisterName;
