import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios"
import { message } from 'antd';
import { Row, Col } from 'reactstrap';

function ViewLawFirm(props) {
    const [lawdata, setlawdata] = useState([])


    useEffect(() => {
        axios.get('https://counsel.apimachine.com/api/getlawfirmdetails/' + props.gettheid)
        // props.gettheid
            .then(response => {
                if (response.data.success) {
                    setlawdata(response.data.data)
                } else {
                    message.error("Error while fetching data! Please try again")
                }
            }).catch(err => console.log(err))
    }, [props.gettheid])
    console.log(JSON.stringify(lawdata) + " full-law-data")
    if(lawdata.length === 0){
        return <div>
            <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">View Law Firm Details</span>
        </div>
        <p>Loading....</p>
        </div>
    }

    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">View Law Firm Details</span>
        </div>
        <div>
            <img src={lawdata.CoverPhoto} className="coverprofile-sty" alt="coverphoto" />
            <img src={lawdata.Logo} className="profileimage-sty12" alt="profileimage" />
            <h6 className="usrnm-styee">{lawdata.Name}</h6>
            <hr />
        </div>
        <div className = "crd-style thepadsty">
        <div className="btn-styy">
                        <button type="button" className="button-sty1" onClick={() => {props.openspecialization(); props.sendlawprofileid(lawdata._id)}}>Add Specializations</button>
                        <button type="button" className="button-sty1" >View Specializations</button>
                    </div>
        </div>

        {/* <p>{props.gettheid}</p>
        <p>Test Law Firm</p> */}
        <div className="crd-style">
            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Address</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                        <p>{lawdata.Address}</p>
                    </Col>
                </Row>
            </div>

            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Email</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                        <p>{lawdata.Email}</p>
                    </Col>
                </Row>
            </div>
            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Website</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                        <p>{lawdata.Website}</p>
                    </Col>
                </Row>
            </div>
            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Phone Number</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                        <p>{lawdata.PhoneNumber}</p>
                    </Col>
                </Row>
            </div>
            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Fax</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                        <p>{lawdata.Fax}</p>
                    </Col>
                </Row>
            </div>
            <div className="thetitlesty">
                <Row>
                    <Col sm="4">
                        <p className="thekeysty ">Additional Links</p>
                    </Col>
                    <Col sm="1">:</Col>
                    <Col sm="7">
                    {lawdata.AdditionalLinks.map(element1 => {
                                console.log(lawdata.AdditionalLinks.length)
                                return <p>{element1.Link}</p>
                            })}
                    </Col>
                </Row>
            </div>
        </div>
    </div>
}

export default ViewLawFirm;
