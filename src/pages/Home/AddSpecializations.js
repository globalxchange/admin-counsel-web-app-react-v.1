import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios"
import { message } from 'antd';
import { Select } from 'antd';
const { Option } = Select;
function onChange(value) {
    console.log(`selected ${value}`);
}

function onBlur() {
    console.log('blur');
}

function onFocus() {
    console.log('focus');
}

function onSearch(val) {
    console.log('search:', val);
}


function AddSpecializations(props) {
    const [BioForSpecialization, setBioForSpecialization] = useState('')

    const handleChange = (e) => {
        setBioForSpecialization(e.target.value)
    }

    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add Specializations</span>
        </div>
        <p>Add Specializations</p>
        <p>{props.lawfirm}</p>
        <div className="crd-style">
            <p className="adjust-title">Bio For Specialization</p>
            <textarea style={{ marginLeft: "25px", border: "none", width: "480px" }} placeholder="Enter Bio..." className="inputtxtarea" rows="4" cols="50" value={BioForSpecialization} name="address" onChange={handleChange} required></textarea>
        </div>
        <div className="crd-style">
            <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select Specialization"
                optionFilterProp="children"
                onChange={onChange}
                onFocus={onFocus}
                onBlur={onBlur}
                onSearch={onSearch}
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
            >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
            </Select>,
            </div>
    </div>
}

export default AddSpecializations;
