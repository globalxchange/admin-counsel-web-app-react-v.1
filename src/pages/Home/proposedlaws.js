import React, { useState, useEffect } from 'react';
import axios from "axios";
import { message } from 'antd';
import { Row, Col } from 'reactstrap';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import CloseImg from "../../static/images/sidebar-icons/closeimg.png"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faWindowClose, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Modal, Button } from 'antd';
import { ReactComponent as PlayBtn } from "../../static/images/sidebar-icons/playbtn.svg"
import { ReactComponent as FullScreenBtn } from "../../static/images/sidebar-icons/fullscreen.svg"
import { ReactComponent as PauseBtn } from "../../static/images/sidebar-icons/pause.svg"
import Interweave from 'interweave';
import ReactPlayer from 'react-player';
import { findDOMNode } from 'react-dom'
import screenfull from 'screenfull'
import jwt from "jsonwebtoken";



function ProposedLaws(props) {
    const [proposelaw, setproposelaw] = useState([])
    const [theid, setid] = useState("")
    const [visible, setVisible] = useState(false)
    const [video, setVideo] = useState(false)
    const [data, setdata] = useState("")
    const [images, setimages] = useState([])
    const [videoid, setvideoid] = useState("")
    const [playing, setplaying] = useState(true)
    const [theplayer, setplayer] = useState('')
    const [played, setplayed] = useState(0)
    const [seeking, setseeking] = useState(false)
    const [seeker, setseeker] = useState("none")
    const [mainbtn, setmainbtn] = useState("")
    const [playerbtn, setplayerbtn] = useState("none")
    const [fullscrbtn, setfullscrbtn] = useState("none")
    const [thumbnail, setthumbnail] = useState("")
    const [videourl, setvideourl] = useState("")
    const [textbtn, settextbtn] = useState("none")
    const [theimagedisp, setimagedisp] = useState("none")


    // const [isModalVisible, setIsModalVisible] = useState(false);

    useEffect(() => {
        console.log(props.getlegid)
        setdata(props.thedata)
        axios.get('https://counsel.apimachine.com/api/getlawarticlesbystatusandid/review/' + props.getlegid)
            .then(response => {
                if (response.data.success) {
                    setproposelaw(response.data.data)
                    // response.data.data.forEach(ele=>{
                    //     if(ele.VideoLink){
                    //         setmainbtn("block")
                    //     }else{
                    //         setmainbtn("none")
                    //     }
                    // })
                    // setimages(response.data.data.Images)
                } else {
                    message.error(response.data.message)
                    console.log(props.getlegid)
                }
            }).catch(err => console.log(err))
    }, [props.getlegid])
    console.log(JSON.stringify(proposelaw))

    const theeditpage = (e) => {
        console.log(e.target.value + "thename")
    }
    const deletearticle = (e) => {
        // setVisible(true);
        setid(e.target.value)
        console.log("delete " + e.target.value)

        axios.delete('https://counsel.apimachine.com/api/deletearticle/' + e.target.value).then(res => {
            console.log(res.data)
            if (res.data.success) {
                message.success(res.data.message)
                axios.get('https://counsel.apimachine.com/api/getlawarticlesbystatusandid/review/' + props.getlegid)
                    .then(response => {
                        if (response.data.success) {
                            setproposelaw(response.data.data)

                        } else {
                            message.error(response.data.message)
                            console.log(props.getlegid)
                        }
                    }).catch(err => console.log(err))
            } else {
                message.error(res.data.message)
            }
        }).catch(err => console.log(err))


    };
    const showModal = (e) => {
        setVisible(true)
        console.log("professionalID " + e.target.name)
        proposelaw.forEach(law => {
            if (law.Title === e.target.name) {
                setimages(law.Images)
                if(law.Images.length === 0){
                    setimagedisp("block")
                }else{
                    setimagedisp("none")
                }
            }
        })
    }
    const showVideoModal = async (e) => {
        setVideo(true)
        proposelaw.forEach(async (law) => {
            if (law.Title === e.target.name) {
                console.log("video-id " + law.VideoLink)
                setvideoid(law.VideoLink)
                if (law.VideoLink) {
                    setthumbnail(law.VideoThumbnail)
                    setmainbtn("block")
                    settextbtn("none")
                    const secret = '9xr7w8KCuY2Wmhe89bR'; //secret not to be disclosed anywhere.
                    const token = jwt.sign({}, secret, {
                        algorithm: "HS512",
                        expiresIn: 240, //seconds
                        issuer: "gxervench215",
                    });
                    let stream_link = await axios.post(
                        `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token}`,
                        {
                            "video_id": law.VideoLink
                        },
                        {
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                            },
                        }
                    );

                    console.log(JSON.stringify(stream_link.data) + "thestream-link");
                    if (stream_link.data === "Video not found") {
                        settextbtn("block")
                        setmainbtn("none")
                        setplayerbtn("none")
                        setseeker("none")
                        setfullscrbtn("none")
                    } else {
                        settextbtn("none")
                        setvideourl(stream_link.data)
                        setmainbtn("block")
                        setplayerbtn("block")
                        setseeker("block")
                        setfullscrbtn("block")
                    }
                } else {
                    setmainbtn("none")
                    settextbtn("block")
                    setplayerbtn("none")
                    setseeker("none")
                    setfullscrbtn("none")
                }
            }
        })
    }
    const handleOk = (e) => {
        setVisible(false)
    };
    const handleOk1 = (e) => {
        setVideo(false)
    }
    const handleCancel = e => {
        setVisible(false)
    };
    const handleCancel1 = e => {
        setVideo(false)
    };
    const handlePause = () => {
        console.log('onPause')
        setplaying(false)
    }
    const handlePlay = () => {
        console.log('onPlay')
        setplaying(true)
    }
    const handlePlayPause = () => {
        setplaying(!playing)
    }
    const ref = player => {
        setplayer(player)
    }
    const handleClickFullscreen = () => {
        screenfull.request(findDOMNode(theplayer))
    }
    const handleDuration = (duration) => {
        console.log('onDuration', duration)
        // setduration(duration)
    }
    const handleSeekChange = (e) => {
        setplayed(parseFloat(e.target.value))
    }
    const handleSeekMouseDown = e => {
        setseeking(true)
    }
    const handleSeekMouseUp = e => {
        theplayer.seekTo(parseFloat(e.target.value))
    }
    const handleProgress = state => {
        console.log('onProgress', state)
        setplayed(state.played)
    }
    // if (proposelaw.length != 0) {
    //     proposelaw.forEach(elem => {
    //         if (elem.VideoLink) {
    //             setmainbtn("block")
    //         } else {
    //             setmainbtn("none")
    //         }
    //     })
    // }
    if (proposelaw.length === 0) {
        return <div>Loading...</div>
    }
    console.log(images + "images")
    return <div>
        <div className="main-style">
            {/* <p>{props.getlegid}</p> */}
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Proposed Laws</span>
            {/* {data} */}
        </div>
        {proposelaw.map(element => {
            return <div className="crd-style">
                <p style={{ display: "flex" }} className="the-edit">
                    <button className="thepropsebtn" style={{ border: "none", background: "white" }}
                     onClick={() => { props.editlawarticle(); 
                     props.sendid(element._id); 
                     props.sendtitle(element.Title); 
                     props.sendbody(element.Body); 
                     props.sendprofileimage(element.ProfileImage); 
                     props.sendcoverphoto(element.CoverPhoto) 
                     props.sendvideoname(element.VideoName)
                     props.sendvideothumbnail(element.VideoThumbnail)
                     props.sendvideolink(element.VideoLink)
                     props.sendimages(element.Images)}}>
                         <FontAwesomeIcon icon={faEdit} onClick={theeditpage} />
                         </button>
                         <input onClick={deletearticle} value={element._id} type="image" src={CloseImg} className="theimgs" alt="no_image" /></p>
                <div className="thetitlesty">
                    <img src={element.CoverPhoto} alt="coverimg" className="cover-img" />
                </div>
                <div className="thetitlesty the-align">
                    <img src={element.ProfileImage} alt="profileimg" className="theprofileimg" />
                </div>
                <div className="thetitlesty">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty ">Title</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.Title}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Body</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p><Interweave content={element.Body} /></p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Law Author</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.LawAuthor}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Clauses</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            {element.Clauses.map(element1 => {
                                console.log(element.Clauses.length)
                                return <p>{element1.Clause}</p>
                            })}
                        </Col>
                    </Row>
                </div>
                {element.Tags.map(element2 => {
                    return <div className="adjstyle">
                        <Row>
                            <Col sm="3">
                                <p className="thekeysty">{element2.Tag[0].TagType.charAt(0).toUpperCase()}{element2.Tag[0].TagType.slice(1)}</p>
                            </Col>
                            <Col sm="1">:</Col>
                            <Col sm="8">
                                <p>{element2.Tag[0].TagName.charAt(0).toUpperCase()}{element2.Tag[0].TagName.slice(1)}</p>
                            </Col>
                        </Row>
                    </div>
                })}
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Status</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.Status}</p>
                        </Col>
                    </Row>
                </div>


                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Published</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.PublicationName}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Images</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <input type="button" className="thepropbtn" alt="images" onClick={showModal} style={{ textAlign: "center" }} value="Preview Images" name={element.Title} />
                        </Col>
                    </Row>
                    <Modal title="Images" visible={visible} onOk={handleOk} onCancel={handleCancel}>
                    <p style={{ display: theimagedisp }}>No Images Uploaded</p>
                        {images.map(elem => {
                            return <img className="theimages" src={elem.image} alt="image" />
                        })}
                    </Modal>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Video</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <input type="button" className="thepropbtn" onClick={showVideoModal} style={{ textAlign: "center" }} value="Preview Video" name={element.Title} />
                        </Col>
                    </Row>
                    <Modal title="Video" visible={video} onOk={handleOk1} onCancel={handleCancel1}>
                        <p style={{ display: textbtn }}>Video not available</p>
                        <ReactPlayer
                            style={{ display: mainbtn }}
                            ref={ref}
                            // url="https://www.youtube.com/watch?v=IUN664s7N-c"
                            url = {videourl}
                            light = {thumbnail}
                            // light="https://drivetest.globalxchange.io/gxsharepublic/?full_link=ram.brain.stream/e7fda5806061a24866eaffa9dff75c9c"
                            controls={false}
                            className="theplayer-sty"
                            width="100%"
                            height="310px"
                            onPause={handlePause}
                            playing={playing}
                            onPlay={handlePlay}
                            pip={false}
                            onSeek={e => console.log('onSeek', e)}
                            onProgress={handleProgress}
                            onDuration={handleDuration}
                        />
                        <div >
                            <p onClick={handlePlayPause} style={{ display: playerbtn,marginTop:"-6px" }} className="theplaypausebtn-sty">{playing ? <PauseBtn /> : <PlayBtn />}</p>
                            <p onClick={handleClickFullscreen} style={{ display: fullscrbtn }} className="thefullscrn-btn1 "><FullScreenBtn /></p>
                        </div>
                        <input
                            type='range' min={0} max={0.999999} step='any'
                            className="theplayerseek"
                            value={played}
                            style={{ display: seeker,marginTop:"50px",width:"100%" }}
                            onMouseDown={handleSeekMouseDown}
                            onChange={handleSeekChange}
                            onMouseUp={handleSeekMouseUp}
                        />
                    </Modal>
                </div>
                <br />
            </div>

        })}

    </div>;
}

export default ProposedLaws;
