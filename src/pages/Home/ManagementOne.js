import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function ManagementOne() {
  return <MainLayout active="employees"></MainLayout>;
}

export default ManagementOne;
