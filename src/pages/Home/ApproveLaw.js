import React, { useState,useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import axios from "axios";
import {message} from 'antd';
import { Row, Col } from 'reactstrap';


function ApproveLaw(props) {
    const [approve, setapprove] = useState([])
    useEffect(() => {
        console.log(props.getlegid)
        axios.get('https://counsel.apimachine.com/api/getlawarticlesbystatusandid/published/' + props.getlegid)
            .then(response => {
                if (response.data.success) {
                    setapprove(response.data.data)
                } else {
                    message.error(response.data.message)
                    console.log(props.getlegid)
                }
            }).catch(err => console.log(err))
    }, [props.getlegid])
  return <div>
     <div className="main-style">
            {/* <p>{props.getlegid}</p> */}
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Approved Laws</span>
          {/* <p>{props.getlegid}</p> */}
        </div>
        {approve.map(element => {
            return <div className="crd-style">
                <div className="thetitlesty">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty ">Title</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.Title}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Body</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.Body}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Law Author</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.LawAuthor}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Law Approver</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.LawApprover}</p>
                        </Col>
                    </Row>
                </div>
                <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Clauses</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            {element.Clauses.map(element1 => {
                                console.log(element.Clauses.length)
                                return <p>{element1.Clause}</p>
                            })}
                        </Col>
                    </Row>
                </div>
                {element.Tags.map(element2 => {
                    return <div className="adjstyle">
                        <Row>
                            <Col sm="3">
                                <p className="thekeysty">{element2.Tag[0].TagType.charAt(0).toUpperCase()}{element2.Tag[0].TagType.slice(1)}</p>
                            </Col>
                            <Col sm="1">:</Col>
                            <Col sm="8">
                                <p>{element2.Tag[0].TagName.charAt(0).toUpperCase()}{element2.Tag[0].TagName.slice(1)}</p>
                            </Col>
                        </Row>
                    </div>
                })}
                 <div className="adjstyle">
                    <Row>
                        <Col sm="3">
                            <p className="thekeysty">Status</p>
                        </Col>
                        <Col sm="1">:</Col>
                        <Col sm="8">
                            <p>{element.Status}</p>
                        </Col>
                    </Row>
                </div>
            </div>
            
        })}
  </div>
}
export default ApproveLaw;
