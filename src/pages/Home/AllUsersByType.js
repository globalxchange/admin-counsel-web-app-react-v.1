import React, { useEffect, useState, Profiler } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import Ellipse from "../../static/images/sidebar-icons/logo/Ellipse.png"
import SearchIcon from "../../static/images/sidebar-icons/logo/search.svg"
import axios from "axios"
import { message } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';


function AllUserByType(props) {
    const [Users, setUsers] = useState([])
    const [text, settext] = useState('')
    const handleChange = (e) => {
        e.preventDefault()
        settext(e.target.value)
        console.log("thetext " + text)
    }
    let thedisplay = "inital"
    if (props.type === "Regulator") {
        thedisplay = "none"
    }
    let thebtndisplay = "none"
    let thenewbtn = "initial"
    let thebtmadj = "2rem"
    let top1 = "-20px"
    let top2 = "20px"


    if (props.type === "Legal Professional") {
        thenewbtn = "none"
        top1 = "0px"
        top2 = "-20px"
    }
    // if (props.type === "Admin") {
    //     thenewbtn = "none"
    // }
    if (props.type === "Legal Professional") {
        thebtndisplay = "initial"
        thebtmadj = "0.6rem"
    }
    let userbtn = "inital"
    if(props.type === "User"){
       userbtn = "none"
    }
    if(props.type === "Admin"){
        userbtn = "none"
     }
    let regbtn = "initial"
    if(props.type === "Regulator"){
        regbtn = "none"
    }
    useEffect(() => {
        setUsers([])
        let key
        if (props.type === "User") {
            key = "Normal User"
        } else if (props.type === "Legal Professional") {
            key = "Legal"
        } else if (props.type === "Regulator") {
            key = "Regulator"
        } else if (props.type === "Admin") {
            key = "Admin"
        } else {
            key = "none"
        }
        axios.get('https://counsel.apimachine.com/api/userslistbytype/' + key)
            .then(response => {
                if (response.data.success) {
                    setUsers(response.data.data)
                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }, [props.type])
    const proposedlaw = (e) => {
        e.preventDefault()
        let profileid = e.target.name
        console.log(e.target.name)
        // ()=>{props.getproposedlaw();props.sendobjId(element._id)}
        axios.get("https://counsel.apimachine.com/api/getlawarticlesbystatusandid/review/" + e.target.name)
            .then(res => {
                if (res.data.success) {
                    props.getproposedlaw()
                    props.sendobjId(profileid)
                } else {
                    message.error(res.data.message)
                }
            })
    }
    const approvedlaw = (e) => {
        e.preventDefault()
        let profileid = e.target.name
        console.log(e.target.name)
        // ()=>{props.getproposedlaw();props.sendobjId(element._id)}
        axios.get("https://counsel.apimachine.com/api/getlawarticlesbystatusandid/published/" + e.target.name)
            .then(res => {
                if (res.data.success) {
                    props.getapprovedlaw()
                    props.sendobjId(profileid)
                } else {
                    message.error(res.data.message)
                }
            })
    }
    const checkuserdetails = (e) => {
        let profileid = e.target.name
        e.preventDefault()
        console.log("the-id " + e.target.name)
        axios.get('https://counsel.apimachine.com/api/getuserdetails/' + e.target.name)
            .then(response => {
                if (response.data.success) {
                    props.viewprofile()
                    props.sendId(profileid)
                } else {
                    message.error(response.data.message)
                }
            })
    }
    const checkprofiles = (e) => {
        e.preventDefault()
        let objid = e.target.name
        axios.get('https://counsel.apimachine.com/api/getuserdetailsbyidwithprofile/' + objid)
            .then(response => {
                if (response.data.success) {
                    props.openfullprofile();
                    props.sendobjId(objid);
                    props.thekey(props.type)
                } else {
                    message.error(response.data.message)
                }
            })
    }
    const updtnormal = (e)=>{
        let key
        if (props.type === "User") {
            key = "Normal User"
        } else if (props.type === "Legal Professional") {
            key = "Legal"
        } else if (props.type === "Regulator") {
            key = "Regulator"
        } else if (props.type === "Admin") {
            key = "Admin"
        } else {
            key = "none"
        }
        e.preventDefault()
        console.log("Normal User " + e.target.name)
        axios.put('https://counsel.apimachine.com/api/makenormaluser/' + e.target.name)
            .then(response => {
                if (response.data.success) {
                    message.success(response.data.message)
                    axios.get('https://counsel.apimachine.com/api/userslistbytype/' + key)
                        .then(res => {
                            if (res.data.success) {
                                message.loading("updating user list")
                                setUsers(res.data.data)
                            } else {
                                message.error(res.data.message)
                            }
                        }).catch(err => console.log(err))
                } else {
                    message.error(response.data.message)
                }
            })
    }
    const updtlegal = (e) => {
        let key
        if (props.type === "User") {
            key = "Normal User"
        } else if (props.type === "Legal Professional") {
            key = "Legal"
        } else if (props.type === "Regulator") {
            key = "Regulator"
        } else if (props.type === "Admin") {
            key = "Admin"
        } else {
            key = "none"
        }
        e.preventDefault()
        console.log("legal professional " + e.target.name)
        axios.put('https://counsel.apimachine.com/api/makelegalprofessional/' + e.target.name)
            .then(response => {
                if (response.data.success) {
                    message.success(response.data.message)
                    axios.get('https://counsel.apimachine.com/api/userslistbytype/' + key)
                        .then(res => {
                            if (res.data.success) {
                                message.loading("updating user list")
                                setUsers(res.data.data)
                            } else {
                                message.error(res.data.message)
                            }
                        }).catch(err => console.log(err))
                } else {
                    message.error(response.data.message)
                }
            })
    }
    const updtreg = (e)=>{
        let key
        if (props.type === "User") {
            key = "Normal User"
        } else if (props.type === "Legal Professional") {
            key = "Legal"
        } else if (props.type === "Regulator") {
            key = "Regulator"
        } else if (props.type === "Admin") {
            key = "Admin"
        } else {
            key = "none"
        }
        e.preventDefault()
        console.log("regulator " + e.target.name)
        axios.put('https://counsel.apimachine.com/api/makeregulator/' + e.target.name)
            .then(response => {
                if (response.data.success) {
                    message.success(response.data.message)
                    axios.get('https://counsel.apimachine.com/api/userslistbytype/' + key)
                        .then(res => {
                            if (res.data.success) {
                                message.loading("updating user list")
                                setUsers(res.data.data)
                            } else {
                                message.error(res.data.message)
                            }
                        }).catch(err => console.log(err))
                } else {
                    message.error(response.data.message)
                }
            })
    }
    let fltrusr = []
    Users.forEach(element => {
        if (element.username.toLowerCase().includes(text.toLowerCase())) {
            fltrusr.push({ _id: element._id, username: element.username, email: element.email, userType: element.userType, profile_id: element.profile_id })
        }
    })
    console.log("filter-user " + JSON.stringify(fltrusr))
    let title = "All " + props.type + "s"


    if (fltrusr.length === 0) {
        return <div>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">{title}</span>
            </div>
            <div style={{ display: "flex" }}>
                <input type="text" value={text} className="search-sty" placeholder="search" onChange={handleChange} />
                <button type="button" className="button-search-sty">All Users</button>
                <button type="button" className="search-ico-sty"><img src={SearchIcon} alt="&nbsp;&nbsp;&nbsp;search" /></button>
            </div>
            <p className="empty-sty">{props.type} not found</p>
        </div>
    }
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">{title}</span>
        </div>
        <div style={{ display: "flex" }}>
            <input type="text" value={text} className="search-sty" placeholder="&nbsp;&nbsp;&nbsp;search" onChange={handleChange} />
            <button type="button" className="button-search-sty">All Users</button>
            <button type="button" className="search-ico-sty"><img src={SearchIcon} alt="search" /></button>
        </div>
        {fltrusr.map(element => {
            console.log("profileid " + element.profile_id)
            console.log(element._id + "theid")
            let theusertype
            if (element.userType === undefined) {
                theusertype = "Normal User"
            } else {
                theusertype = element.userType
            }
            return <div key={element._id} className="crd-style" >
                <div className="titleimg-sty" style={{ display: "flex" }}>
                    <img src={Ellipse} alt="swag" className="img-sty img-dimen" />
                    <div>
                        <p className="title-sty title-adj">{element.username}</p><p className="balance-sty">$21,356.35</p>
                        <p className="email-adj">{element.email}</p><p className="balance-titlesty">Total balance</p>
                    </div>
                </div>
                <p className="body-sty sty1">User ID&nbsp;:&nbsp;<span className="profile-sty">{element._id}</span></p>
                <p className="body-sty">User Type&nbsp;:&nbsp;<span className="profile-sty">{theusertype}</span></p>

                <div className="btn-styy">
                    <button type="button" style={{ marginBottom: thebtmadj }} className="button-sty1 ">Interact</button>
                    <button type="button" style={{ display: thedisplay, marginBottom: thebtmadj }} className="button-sty1 " onClick={() => { props.addtheprofiles(); props.setId(element.profile_id); props.thekey(props.type) }}>Add Profile</button>
                    <button type="button" style={{ display: thedisplay, marginBottom: thebtmadj }} name={element._id} className="button-sty1 " onClick={checkprofiles}>Full User Profile</button>
                    {/* () => {props.openfullprofile(); props.sendobjId(element._id); props.thekey(props.type)} */}
                    {/* <button type="button" className="button-sty1" onClick={() => { props.addprofile(); props.setId(element._id) }}>Add Details</button> */}
                    {/* <button type="button" className="button-sty1" name = {element._id} onClick = {checkuserdetails}>Get Details</button> */}
                </div>
                <div className="btn-styy" style={{ marginTop: top1 }}> {/*0*/}
                    <button type="button" style={{ display: userbtn }} className="button-sty1" name={element._id} onClick={updtnormal}>Make Normal User</button>
                    <button type="button" style={{ display: thenewbtn }} className="button-sty1" name={element._id} onClick={updtlegal}>Make Legal Professional</button>
                    <button type="button" style={{ display: regbtn }} className="button-sty1" name={element._id} onClick={updtreg}>Make Regulator</button>
                </div>
                <div className="btn-styy" style={{ marginTop: top2 }}>{/*-20*/}
                    <button type="button" style={{ display: thebtndisplay }} className="button-sty1" onClick={() => { props.addlaw(); props.sendobjId(element._id) }}>Create Law</button>
                    <button type="button" style={{ display: thebtndisplay }} className="button-sty1" name={element._id} onClick={proposedlaw}>Proposed Laws</button>
                    <button type="button" style={{ display: thebtndisplay }} className="button-sty1" name={element._id} onClick={approvedlaw}>Approved Laws</button>
                </div>
            </div>
        })}
    </div>
}

export default AllUserByType;
