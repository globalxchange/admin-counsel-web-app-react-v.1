import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import SwagImg from "../../static/images/sidebar-icons/logo/swag.png"
import "./pages.scss"
import axios from "axios"

function WorkSpaceOne(props) {
  const [apidata, setapidata] = useState([])
  // let [users, setusers] = useState('userscount' || 0)
  console.log(props.getthedata + " toincrease-count")
  const getapi = (e) => {
    axios.get('https://counsel.apimachine.com/api/counselappuserslist')
      .then(response => {
        setapidata(response.data.data)
      }).catch(err => console.log(err))
  }
  useEffect(() => {
    setapidata([])
    getapi()
  }, [])
  let normalusers = 0;
  let legal = 0;
  let regulators = 0;
  let admins = 0
  apidata.forEach(element => {
    if (element.userType == null || element.userType === "Admin") {
      normalusers += 1;
    }
    if (element.userType === "Legal Professional") {
      legal += 1;
    }
    if (element.userType === "Regulator") {
      regulators += 1;
    }
    if (element.userType === "Admin") {
      admins += 1
    }
  });
  useEffect(() => {
    localStorage.setItem('userscount', normalusers);
  }, [normalusers]);
  if(props.getthedata === "User"){
    localStorage.getItem('userscount' || 0)
    normalusers += 1;
  }
  if(props.getthedata === "Legal Professional"){
    legal += 1;
  }
  if(props.getthedata === "Regulator"){
    regulators += 1;
  }

  return (
    <div>
      <div className="main-style">
        <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onCloseall()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add New</span>
      </div>
      <div className="crd-style" >
        <div className="titleimg-sty">
          <img src={SwagImg} alt="swag" className="img-sty" />
          <span className="maintitle-sty">Users</span>
          <span className="thecount">{normalusers}</span>
        </div>
        <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
        <div>
          <button type="button" className="button-sty sty1">Invite</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose(); props.setVariable1("User") }} >Manual</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose13(); props.setVariable2("User") }}>See User List</button>
        </div>
      </div>
      <div className="crd-style" >
        <div className="titleimg-sty">
          <img src={SwagImg} alt="swag" className="img-sty" />
          <span className="maintitle-sty">Legal Professionals</span>
          <span className="thecount">{legal}</span>
        </div>
        <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
        <div>
          <button type="button" className="button-sty sty1">Invite</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose(); props.setVariable1("Legal Professional") }}>Manual</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose13(); props.setVariable2("Legal Professional") }}> See User List</button>
        </div>
      </div>
      <div className="crd-style" >
        <div className="titleimg-sty">
          <img src={SwagImg} alt="swag" className="img-sty" />
          <span className="maintitle-sty">Regulators</span>
          <span className="thecount">{regulators}</span>
        </div>
        <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
        <div>
          <button type="button" className="button-sty sty1">Invite</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose(); props.setVariable1("Regulator") }}>Manual</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose13(); props.setVariable2("Regulator") }}>See User List</button>
        </div>
      </div>
      <div className="crd-style sty2">
        <div className="titleimg-sty">
          <img src={SwagImg} alt="swag" className="img-sty" />
          <span className="maintitle-sty">Admins</span>
          <span className="thecount">{admins}</span>
        </div>
        <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
        <div>
          <button type="button" className="button-sty sty1">Invite</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose12() }}>Manual</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose13(); props.setVariable2("Admin") }}>See User List</button>
        </div>
      </div>
    </div>
  )
}
export default WorkSpaceOne;
