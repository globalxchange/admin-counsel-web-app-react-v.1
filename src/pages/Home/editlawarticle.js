import React, { useState, useEffect } from 'react';
import axios from "axios";
import { Modal, message } from 'antd';
import { Row, Col } from 'reactstrap';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faWindowClose, faEdit, faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons';
import { Form, Input, InputNumber, Button, Card, Space, Select } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import Axios from 'axios';
import jwt from "jsonwebtoken";
import _ from 'underscore'
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import ReactPlayer from 'react-player';
import { findDOMNode } from 'react-dom'
import screenfull from 'screenfull'
import { ReactComponent as PlayBtn } from "../../static/images/sidebar-icons/playbtn.svg"
import { ReactComponent as FullScreenBtn } from "../../static/images/sidebar-icons/fullscreen.svg"
import { ReactComponent as PauseBtn } from "../../static/images/sidebar-icons/pause.svg"
import CloseImg from "../../static/images/sidebar-icons/closeimg.png"
import { Tooltip } from 'antd';



function EditLawArticle(props) {
    const [proposelaw, setproposelaw] = useState("")
    const [clauses, setclauses] = useState([])
    const [tags, settags] = useState([])
    const [thealltags, setalltags] = useState([])
    const [title, settitle] = useState("")
    const [loading, setloading] = useState(false)
    const [body, setbody] = useState("")
    const [thefile, setfile] = useState("none")
    const [coverfile, setcoverfile] = useState("none")
    const [profilephoto, setprofilephoto] = useState("")
    const [coverphoto, setcoverphoto] = useState("")
    const [videoname, setvideoname] = useState("")
    const [thumbnail, setthumbnail] = useState("")
    const [thelink, setlink] = useState("")
    const [theplayer, setplayer] = useState('')
    const [playing, setplaying] = useState(true)
    const [seeking, setseeking] = useState(false)
    const [mainbtn, setmainbtn] = useState("none")
    const [playerbtn, setplayerbtn] = useState("none")
    const [fullscrbtn, setfullscrbtn] = useState("none")
    const [seeker, setseeker] = useState("none")
    const [thevideoid, setvideoid] = useState("")
    const [images, setimages] = useState([])
    const [videodisp, setvideodisp] = useState("none")
    const [streamlink, setstreamlink] = useState("")
    const [thevideo, setVideo] = useState(false)
    const [preview, setpreview] = useState("none")
    const [nodisp, setnodisp] = useState("none")
    const [theimage, settheimage] = useState(false)
    const [played, setplayed] = useState(0)
    const [imagetitle, setimagetitle] = useState("none")
    const [visible3, setvisible3] = useState(false)
    const [imagelength, setimagelength] = useState([])







    useEffect(() => {
        console.log(props.getid)
        settitle(props.title)
        setbody(props.body)
        setprofilephoto(props.image)
        setcoverphoto(props.photo)
        // setimages(props.allimages)
        // setvideoname(props.videoname)
        // setthumbnail(props.thumbnail)
        // setvideoid(props.thelink)
        console.log("video-lnk" + props.thelink)
        if (props.allimages === null || props.allimages.length === 0) {
            setimages([])
            setimagetitle("none")
        } else {
            setimages(props.allimages)
            setimagetitle("block")
        }

        if (props.videoname === null) {
            setvideoname("")
        } else {
            setvideoname(props.videoname)
        }

        if (props.thumbnail === null) {
            setthumbnail("")
        } else {
            setthumbnail(props.thumbnail)
        }

        if (props.thelink === null) {
            setvideoid("")
        } else {
            setvideoid(props.thelink)
        }
        if (props.thelink === undefined || props.thelink === "" || props.thelink === null) {
            setvideodisp("none")
            setpreview("none")
        } else {
            setvideodisp("block")
            setpreview("block")
        }
        // props.getid
        axios.get('https://counsel.apimachine.com/api/getlawarticle/' + props.getid)
            .then(response => {
                if (response.data.success) {
                    setproposelaw(response.data.data)
                    setclauses(response.data.data.Clauses)
                    // settags(response.data.data.Tags)
                } else {
                    message.error(response.data.message)
                    console.log(props.getlegid)
                }
            }).catch(err => console.log(err))

        axios.get('https://counsel.apimachine.com/api/getalltagnames/' + props.getid)
            .then(response => {
                if (response.data.success) {
                    settags(response.data.data)
                } else {
                    message.error(response.data.message)
                    console.log(props.getlegid)
                }
            }).catch(err => console.log(err))

        //     const secret = '9xr7w8KCuY2Wmhe89bR'
        //     const token = jwt.sign({}, secret, {
        //         algorithm: "HS512",
        //         expiresIn: 240, //seconds
        //         issuer: "gxervench215",
        //       });
        //       let stream_link= await axios.post(
        //         `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token}`,
        //       {
        //                       "video_id": "3b475a08-05bc-4244-a402-7a794ba34de0"
        //                  },
        //       {
        //         headers: {
        //           "Access-Control-Allow-Origin": "*",
        //         },
        //       }
        //     );

        //   console.log(stream_link);
        //   setlink(stream_link.data)
    }, [props.getid])



    console.log("props data " + title + " " + body + " " + profilephoto + " " + coverphoto)
    const handleChange = (e) => {
        setproposelaw(e.target.value);
        if (e.target.name === "title") {
            settitle(e.target.value)
        } else if (e.target.name === "body") {
            setbody(e.target.value)
        } else if (e.target.name === "videoname") {
            setvideoname(e.target.value)
        } else {
            message.error("error while adding input")
        }
    }
    const handleChangeEdit = (value) => {
        setbody(value)
    }
    const handleChange2 = (e, index) => {
        const { name, value } = e.target;
        const list = [...clauses];
        list[index][name] = value;
        setclauses(list);
    }
    const handleChangetags = (e, index) => {
        const { name, value } = e.target;
        const thelist = [...tags];
        thelist[index][name] = value;
        settags(thelist);
    }
    const handlePause = () => {
        console.log('onPause')
        setplaying(false)
    }
    const handlePlay = () => {
        console.log('onPlay')
        setplaying(true)
    }
    const handlePlayPause = () => {
        setplaying(!playing)
    }
    const ref = player => {
        setplayer(player)
    }
    const handleClickFullscreen = () => {
        screenfull.request(findDOMNode(theplayer))
    }
    const handleDuration = (duration) => {
        console.log('onDuration', duration)
        // setduration(duration)
    }
    const handleSeekChange = (e) => {
        setplayed(parseFloat(e.target.value))
    }

    const handleSeekMouseDown = e => {
        setseeking(true)
    }
    const handleSeekMouseUp = e => {
        theplayer.seekTo(parseFloat(e.target.value))
    }
    const handleProgress = state => {
        console.log('onProgress', state)
        setplayed(state.played)
    }
    const setnewmodal = (e) => {
        setvisible3(true)
    }
    // const handleInputChange = (e, index) => {
    //     const { name, value } = e.target;
    //     const list = [...inputList];
    //     list[index][name] = value;
    //     setInputList(list);
    // };
    // const handleRemoveClick = index => {
    //     const list = [...inputList];
    //     list.splice(index, 1);
    //     setInputList(list);
    // };
    // const handleAddClick = () => {
    //     setInputList([...inputList, { Clause: "" }]);
    // };
    const videoupload = async (e) => {
        e.preventDefault()
        const hide = message.loading("Uploading Video", 0)
        let filename = e.target.files[0].name
        let selected_file = e.target.files[0]
        let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
        const formData = new FormData();
        formData.append("files", selected_file)

        const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
        const email = "ram@nvestbank.com"; //email of the developer.
        const path_inside_brain = "root/CounselAppImages/";

        const title1 = "Title of video";
        const subtitle = "Title of video";
        const video_description = "Description of the video";
        const thumbnailUrl = thumbnail //image url of the thumbail that is already uplaoded to brain.
        console.log(thumbnail + "url of thumbnail")

        const token = jwt.sign({ name: fileName, email: email }, secret, {
            algorithm: "HS512",
            expiresIn: 240,
            issuer: "gxjwtenchs512",
        });


        let response = await Axios.post(
            `https://drivetest.globalxchange.io/file/dev-upload-video?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}&title=${title1}&subtitle=${subtitle}&video_description=${video_description}&thumbnailUrl=${thumbnailUrl}`,
            formData,
            {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                },
            }
        );

        console.log(response.data);
        if (response.data.status) {
            setvideoid(response.data.payload.video_id)
            const secret1 = '9xr7w8KCuY2Wmhe89bR'; //secret not to be disclosed anywhere.
            const token1 = jwt.sign({}, secret1, {
                algorithm: "HS512",
                expiresIn: 240, //seconds
                issuer: "gxervench215",
            });
            let stream_link = await Axios.post(
                `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token1}`,
                {
                    "video_id": response.data.payload.video_id
                },
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );
            console.log("videolink11 " + JSON.stringify(stream_link));
            console.log("video-url " + stream_link.data)
            setTimeout(hide, 100);
            setvideodisp("block")
            // setplayerbtn("block")
            // setfullscrbtn("block")
            // setseeker("block")
            setstreamlink(stream_link.data)
            setpreview("block")
            // setimgdisp("none")
            console.log("video-length " + stream_link.content_length)
        } else {
            message.error(response.data.payload)
        }

    }
    let handleChange1 = async (e) => {
        e.preventDefault()
        if (e.target.files[0] === undefined) {
            message.error("Please upload files")
        } else {
            let filename = e.target.files[0].name
            console.log(e.target.files[0])
            console.log("name-file " + filename)
            let name = e.target.name
            console.log("name " + name)

            // let fileName = e.target.files[0].name
            let fileext = e.target.files[0].type
            let selected_file = e.target.files[0]
            console.log("file-type " + fileext)
            if (name === "profile" || name === "cover" || name === "thumbnail") {
                if (fileext.substring(0, 5) === "image") {
                    const formData = new FormData();
                    formData.append("files", selected_file)
                    const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                    const email = "ram@nvestbank.com"; //email of the developer.
                    const path_inside_brain = "root/CounselAppImages/";
                    let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                    console.log(fileName)
                    const token = jwt.sign({ name: fileName, email: email }, secret, {
                        algorithm: "HS512",
                        expiresIn: 240,
                        issuer: "gxjwtenchs512",
                    });

                    console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
                    let response = await Axios.post(
                        `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                        formData,
                        {
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                            },
                        }
                    );

                    console.log(JSON.stringify(response.data) + " image-upload");
                    console.log(selected_file.name + " imagename")

                    if (response.data.status) {
                        if (name === "profile") {
                            setfile("block")
                            setprofilephoto(response.data.payload.url)
                        } else if (name === "cover") {
                            setcoverfile("block")
                            setcoverphoto(response.data.payload.url)
                        } else if (name === "thumbnail") {
                            // setthumbfile("block")
                            setthumbnail(response.data.payload.url)

                        }
                    } else {
                        message.error(response.data.payload)
                    }
                } else {
                    message.error("please upload only images")
                }
            }
        }
    }
    const showVideoModal = async (e) => {
        setVideo(true)
        const secret1 = '9xr7w8KCuY2Wmhe89bR'; //secret not to be disclosed anywhere.
        const token1 = jwt.sign({}, secret1, {
            algorithm: "HS512",
            expiresIn: 240, //seconds
            issuer: "gxervench215",
        });
        let stream_link = await Axios.post(
            `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token1}`,
            {
                "video_id": thevideoid
            },
            {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                },
            }
        );
        console.log("videolink1 " + JSON.stringify(stream_link));
        console.log("video-url1 " + stream_link.data)
        // setvideodisp("block")
        if (stream_link.data === "Video not found") {
            setvideodisp("none")
            setnodisp("block")
        } else {
            setstreamlink(stream_link.data)
            setnodisp("none")
            setvideodisp("block")
        }
    }
    const captureimage = (e) => {
        console.log("imageid " + e.target.value)
        const items = images.filter(item => item.image !== e.target.value);
        console.log(JSON.stringify(items))
        setimages(items)
        if(items.length === 0){
            setimagetitle("none")
        }
    }
    let uploadimages = async (e) => {
        e.preventDefault()
        let selectedFiles = e.target.files
        // let Images = []
        // let filename
        // let selected_file
        console.log("files  " + selectedFiles.length)
        for (var i = 0; i < selectedFiles.length; i++) {
            const formData = new FormData
            const hide = message.loading("Uploading Images", 0)
            let fileName = selectedFiles[i].name
            console.log("name-of-file " + selectedFiles[i].name)
            let selected_file = selectedFiles[i]
            formData.append("files", selected_file)
            console.log("formdata " + (formData))
            const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf";
            const email = "ram@nvestbank.com";
            const path_inside_brain = "root/CounselAppImages/";
            // let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);

            const token = jwt.sign({ name: fileName, email: email }, secret, {
                algorithm: "HS512",
                expiresIn: 240,
                issuer: "gxjwtenchs512",
            });
            // console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
            let response = await Axios.post(
                `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                formData,
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );
            console.log(response.data)
            if (response.data.status) {
                images.push({ image: response.data.payload.url })
                imagelength.push({ image: response.data.payload.url })
                setTimeout(hide, 100);
            } else {
                setTimeout(hide, 100);
                // message.error(response.data.payload)
                console.log("error")
            }
            console.log("Images " + JSON.stringify(images))
            // console.log("New ImaGE " + JSON.stringify(newimages))
        }
        if (selectedFiles.length === imagelength.length) {
            message.success("Images uploaded")
            setimagetitle("block")
        } else {
            message.error("One or more images not uploaded, please try again")
        }
    }
    const handleOk1 = (e) => {
        setVideo(false)
    }
    const handleCancel1 = e => {
        setVideo(false)
    };
    const showImageModal = (e) => {
        settheimage(true)
    }
    const handleOk2 = (e) => {
        settheimage(false)
        console.log("images12 " + JSON.stringify(images))
    }
    const handleCancel2 = (e) => {
        settheimage(false)
    }
    const handleOk3 = (e) => {
        setvisible3(false)
    }
    const handleCancel3 = (e) => {
        setvisible3(false)
    }
    const onFinish = e => {
        e.preventDefault()
        // if(title === ""){
        //     settitle(proposelaw.Title)
        // }
        // if(body === ""){
        //     setbody(proposelaw.Body)
        // }
        // if(profilephoto === ""){
        //     setprofilephoto(proposelaw.ProfileImage)
        // }
        // if(coverphoto === ""){
        //     setcoverphoto(proposelaw.CoverPhoto)
        // }
        console.log(e);
        console.log(title, body)
        let Tags = []
        for (let i = 0; i < tags.length; i++) {
            Tags.push({ Tag: [{ TagType: tags[i].tagtype, TagName: tags[i].tagname }] })
        }
        // console.log(JSON.stringify(e.users))
        // console.log(JSON.stringify(Tags))
        let body1 = {
            "Title": title,
            "Body": body,
            "Clauses": clauses,
            "Tags": Tags,
            "ProfileImage": profilephoto,
            "CoverPhoto": coverphoto,
            "VideoName": videoname,
            "VideoThumbnail": thumbnail,
            "Images": images,
            "VideoLink": thevideoid
        }
        Axios.put("https://counsel.apimachine.com/api/editarticle/" + props.getid, body1).then(res => {
            if (res.data.success) {
                message.success("Article updated Successfully")
            } else {
                message.error(res.data.message)
            }
        })

        console.log(JSON.stringify(body1))
    };
    // const onFinish1 = values => {
    //     console.log(values);
    // };

    // if (proposelaw === "") {
    //     settitle("")
    // }else{
    //     settitle(proposelaw.Title)
    // }

    // console.log("Tags" + JSON.stringify(tags))
    //     tags.forEach(element => {
    //         thealltags.push({ tagtype: element.Tag[0].TagType, tagname: element.Tag[0].TagName })
    //     })
    // console.log("thealltags " + JSON.stringify(thealltags))
    // let types = thealltags.map(o => o.tagtype)
    // let tagdups = thealltags.filter(({tagtype}, index) => !types.includes(tagtype, index + 1))

    // var tagdups = thealltags.filter((val, id, array) => {
    //     console.log("val" + array.indexOf(val) + "   " + id);
    //     return array.indexOf(val) === id;
    //   });
    //  console.log("tag-dups " + JSON.stringify(tagdups))
    return <div>
        <div className="main-style">
            <input type="image" src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => { props.onClose(); props.send(title) }} />
&nbsp;&nbsp;&nbsp;<span className="add-style">Edit Article</span>
        </div>
        <form onSubmit={onFinish}>
            <div>
                <div className="crd-style">
                    <p className="adjust-title">Title</p>
                    <input type="text" name="title" className="inputtitle" placeholder="Ex: law firm" value={proposelaw.Title} onChange={handleChange} required />
                </div>
                <div className="crd-style">
                    <p className="adjust-title">Body</p>
                    <ReactQuill style={{ marginLeft: "25px", border: "none", width: "480px" }} rows="4" cols="50" value={body} onChange={handleChangeEdit} />
                    {/* <textarea style={{ marginLeft: "25px", border: "none", width: "480px" }} placeholder="Enter description..." className="inputtxtarea" rows="4" cols="50" value={proposelaw.Body} name="body" onChange={handleChange} required></textarea> */}
                </div>
                <div className="crd-style">
                    <p className="adjust-title">Upload Profile Photo</p><br />
                    <div style={{ display: "flex" }}>
                        <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="profile" onChange={handleChange1} />
                        <img src={profilephoto} className=" img-stye" alt="profilephoto" />
                    </div>
                </div>
                <div className="crd-style">
                    <p className="adjust-title">Upload Cover Photo</p><br />
                    <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="cover" onChange={handleChange1} />
                    <img src={coverphoto} className="img-stye1" alt="coverphoto" />
                </div>
                <div className="crd-style">
                    <p className="adjust-title">Clauses</p>
                    {clauses.map((x, i) => {
                        return <div style={{ display: "flex" }}>
                            <p className="theclausesty">Clause</p>
                            <input
                                name="Clause"
                                placeholder="Clause"
                                className="theinpclausesty"
                                value={x.Clause}
                                onChange={e => handleChange2(e, i)}
                            />
                        </div>
                    })}
                </div>
                <div className="crd-style">
                    <p className="adjust-title">Tags</p><br />
                    {tags.map((x, i) => {
                        return <div>
                            <Row>
                                <Col sm="3">
                                    <p className="theclausesty">{x.tagtype.charAt(0).toUpperCase()}{x.tagtype.slice(1)}</p>
                                </Col>
                                <Col sm="1"></Col>
                                <Col sm="8">
                                    <input
                                        name="tagname"
                                        placeholder="tagname"
                                        className="theinpclausesty"
                                        value={x.tagname}
                                        onChange={e => handleChangetags(e, i)}
                                    />
                                </Col>
                            </Row>
                        </div>
                    })}
                </div>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Images</p>
                <input type="button" className="thepropbtn" onClick={showImageModal} style={{ textAlign: "center", marginTop: "10px" }} value="Preview Images" />
                <Modal title="Add/Remove Images" visible={theimage} onOk={handleOk2} onCancel={handleCancel2}>
                    {/* <p style={{ display: imagetitle }}>No Images found</p> */}
                    <Tooltip placement="bottom" title="Upload Images">
                    <div className="the-border" onClick={setnewmodal}>
                            <FontAwesomeIcon icon={faCloudUploadAlt} />
                    </div>
                    </Tooltip>
                    <div><br/>
                    <h6 style = {{display: imagetitle}}>View Images</h6>
                        {images.map(element => {
                            return <>
                                <img src={element.image} className="img-styemodal" />
                                <input value={element.image} type="image" src={CloseImg} className="theimgclose" alt="no_image" onClick={captureimage} />
                            </>
                        })}

                        <Modal title="Add Images" visible={visible3} onOk={handleOk3} onCancel={handleCancel3}>
                            <input type="file" style={{ color: "transparent" }} className="theimage-styy1" name="cover" multiple onChange={uploadimages} />
                        </Modal>
                    </div>
                </Modal>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Video Name</p>
                <input type="text" name="videoname" className="inputtitle" placeholder="Enter name of the video" value={videoname} onChange={handleChange} />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Video Thumbnail</p><br />
                <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="thumbnail" onChange={handleChange1} />
                <img src={thumbnail} className="img-stye1" />
            </div>

            <div className="crd-style">
                <p className="adjust-title">Upload Video:</p><br />
                <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="video" onChange={videoupload} />
                <input type="button" className="thepropbtn" onClick={showVideoModal} style={{ textAlign: "center", display: preview }} value="Preview Video" />
                <Modal title="Video" visible={thevideo} onOk={handleOk1} onCancel={handleCancel1}>
                    <p style={{ display: nodisp }}>Video not available</p>
                    <ReactPlayer
                        style={{ display: videodisp }}
                        ref={ref}
                        url={streamlink}
                        light={thumbnail}
                        controls={false}
                        className="theplayer-sty"
                        width="100%"
                        height="310px"
                        onPause={handlePause}
                        playing={playing}
                        onPlay={handlePlay}
                        pip={false}
                        onSeek={e => console.log('onSeek', e)}
                        onProgress={handleProgress}
                        onDuration={handleDuration}
                    />
                    <div >
                        <p onClick={handlePlayPause}
                            style={{ display: videodisp, marginTop: "-6px" }}
                            className="theplaypausebtn-sty">{playing ? <PauseBtn /> : <PlayBtn />}</p>
                        <p onClick={handleClickFullscreen}
                            style={{ display: videodisp }}
                            className="thefullscrn-btn1"><FullScreenBtn /></p>
                    </div>
                    <input
                        type='range' min={0} max={0.999999} step='any'
                        className="theplayerseek"
                        value={played}
                        style={{ display: videodisp, marginTop: "50px", width: "100%" }}
                        onMouseDown={handleSeekMouseDown}
                        onChange={handleSeekChange}
                        onMouseUp={handleSeekMouseUp}
                    />
                </Modal>
            </div>

            <button type="submit" style={{ position: "initial" }}
                className="footer btmste"
                disabled={loading}
            >
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
            </button>

            {/* {inputList.map((x, i) => {
                        return (
                            <div>
                                <div style={{ display: "flex" }}>
                                    <p className="theclausesty">Clause</p>
                                    <input
                                        name="Clause"
                                        placeholder="Clause"
                                        className="theinpclausesty"
                                        value={x.Clause}
                                        onChange={e => handleInputChange(e, i)}
                                    />
                                    {inputList.length !== 1 && <MinusCircleOutlined
                                        className="mr10 theminusbtn"
                                        onClick={() => handleRemoveClick(i)} />}
                                    <div>
                                    </div>
                                </div>
                                {inputList.length - 1 === i && <Button className="addclausebtn" type="dashed" onClick={handleAddClick}><PlusOutlined /> Add Clause</Button>}
                            </div>
                        );
                    })} */}
        </form>
    </div >
}

export default EditLawArticle;
