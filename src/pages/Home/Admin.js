import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function Admin() {
  return <MainLayout active="Admin"></MainLayout>;
}

export default Admin;
