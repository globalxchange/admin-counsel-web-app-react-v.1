import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function Marketing() {
  return <MainLayout active="marketing"></MainLayout>;
}

export default Marketing;
