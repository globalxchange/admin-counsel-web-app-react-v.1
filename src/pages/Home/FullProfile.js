import React, { useState, useEffect } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios"
import { message } from 'antd';



function FullProfile(props) {

    const [activeTab, setActiveTab] = useState('1');
    const [profiledata, setprofiledata] = useState([])

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    useEffect(() => {
        axios.get('https://counsel.apimachine.com/api/getuserdetails/' + props.profileid)
            .then(response => {
                if (response.data.success) {
                    setprofiledata(response.data.data)
                } else {
                    message.error("Error while fetching data! Please try again")
                }
            }).catch(err => console.log(err))
    }, [props.profileid])
    console.log(JSON.stringify(profiledata) + " full-profile-data")
    let Employeedetails = []
    for (let a in profiledata.EmployerInformation) {
        Employeedetails.push(profiledata.EmployerInformation[a])
    }
    console.log('Employer ' + Employeedetails)
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
      &nbsp;&nbsp;&nbsp;<span className="add-style">Full Profile Details</span>
        </div>
        <div>
            <img src={profiledata.CoverPhoto} className="coverprofile-sty" alt="coverphoto" />
            <img src={profiledata.ProfileImage} className="profileimage-sty12" alt="profileimage" />
            <h3 className="usrnm-styee">{profiledata.FirstName}&nbsp;{profiledata.LastName}</h3>
            <hr />
        </div>
        <div>
            <Nav tabs>
                <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === '1' })}
                        onClick={() => { toggle('1'); }}
                    >
                        General Information
          </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === '2' })}
                        onClick={() => { toggle('2'); }}
                    >
                        Employer Information
          </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === '3' })}
                        onClick={() => { toggle('3'); }}
                    >
                        Uploaded Documents
          </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                    <Row>
                        <Col sm="12">
                            <div className="userdtl-stye crd-style">
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Country</p>
                                    <p className="data-stye">{profiledata.Country}</p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">State</p>
                                    <p className="data-stye">{profiledata.State}</p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Phone Number</p>
                                    <p className="data-stye">{profiledata.PhoneNumber}</p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Occupation</p>
                                    <p className="data-stye">{profiledata.Occupation}</p>
                                </p>
                            </div>
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId="2">
                    <div className="userdtl-stye crd-style">
                        <Row>
                            <Col sm="6">
                                <span className="inf-stye">Company Name</span>
                                <p className="data-stye thestye135">{Employeedetails[0]}</p>
                            </Col>
                            <Col sm="6">
                                <span className="inf-stye">Company Number</span>
                                <p className="data-stye thestye135">{Employeedetails[1]}</p>
                            </Col>
                        </Row>
                        <p>
                            <span className="inf-stye">Company Address</span>
                            <p className="data-stye thestye135">{Employeedetails[2]}</p>
                        </p>
                        <p style={{ display: "flex" }}>
                            <p className="inf-stye">Years Working for the Company</p>
                            <p className="data-stye">{Employeedetails[3]}</p>
                        </p>
                    </div>
                </TabPane>
                <TabPane tabId="3">
                    <Row>
                        <Col sm="12">
                            <div className="userdtl-stye crd-style">
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Passport</p>
                                    <p className="data-stye"><a href={profiledata.Passport} target="blank">View/Download</a></p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Birth Certificate</p>
                                    <p className="data-stye"><a href={profiledata.BirthCertificate} target="blank">View/Download</a></p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">License</p>
                                    <p className="data-stye"><a href={profiledata.License} target="blank">View/Download</a></p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Social Insurance Number</p>
                                    <p className="data-stye"><a href={profiledata.SocialInsuranceNumber} target="blank">View/Download</a></p>
                                </p>
                                <p style={{ display: "flex" }}>
                                    <p className="inf-stye">Criminal Background check</p>
                                    <p className="data-stye"><a href={profiledata.CriminalBackgroundCheck} target="blank">View/Download</a></p>
                                </p>
                            </div>
                        </Col>
                    </Row>

                </TabPane>
            </TabContent>
        </div>
    </div>
}

export default FullProfile;
