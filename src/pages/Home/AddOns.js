import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function AddOns() {
  return <MainLayout active="addons"></MainLayout>;
}

export default AddOns;
