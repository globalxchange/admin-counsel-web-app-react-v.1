import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import axios from "axios"
import { Modal, Button } from 'antd';
import message from 'antd/lib/message';



function WorkSpaceThree(props) {
    const [users, setUsers] = useState([])
    const [count, setCount] = useState('')
    const [visible, setVisible] = useState(false)
    const [email, setemail] = useState('')
    const [profileid, setprofileid] = useState('')

    const theget = () => {
        axios.get('https://counsel.apimachine.com/api/userslistbytype/Normal User')
            .then(response => {
                setCount(response.data.Total_user)
                setUsers(response.data.data)
            }).catch(err => console.log(err))
    }

    const showModal = (e) => {
        setVisible(true);
        setemail(e.target.value)
        setprofileid(e.target.name)
    };

    const handleOk = (e) => {
        setVisible(false)
        let body = {
            "_id": "5edf603c4bbacf102bf306ae",
            "profile_id": profileid
        }
        axios.post('https://counsel.apimachine.com/api/addadmin', body).then(res => {
            console.log(res.data)
            if (res.data.success) {
                message.success(res.data.message)
                theget()
            } else {
                message.error(res.data.message)
            }
        }).catch(err => console.log(err))
        console.log("thebody " + JSON.stringify(body))
    };
    const handleCancel = e => {
        setVisible(false)
    };
    let theemail = "Make " + email + " Admin ?"



    useEffect(() => {
        theget()
    }, [])
    let theonlyusers = []
    users.forEach(element => {
        if (element.userType !== "Admin") {
            theonlyusers.push({ email: element.email, profile_id: element.profile_id })
        }
    })
    console.log("theusers" + JSON.stringify(theonlyusers))
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Users List</span>
        </div>

        {theonlyusers.map(element => {
            return <div className="crd-style">
                <p className="adjust-title">Email ID </p>
                <p className="theadmin-sty">{element.email}</p>
                <p className="theadmin-sty">{element.userType}</p>
                <button type="button" style={{marginLeft:"0px",marginBottom: "0px" }} className="button-sty" value={element.email} name={element.profile_id} onClick={showModal}>Add as Admin</button>
            </div>
        })}

        <Modal
            title=""
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <p style={{ textAlign: "center" }}>{theemail}</p>
        </Modal>
    </div>;
}

export default WorkSpaceThree;
