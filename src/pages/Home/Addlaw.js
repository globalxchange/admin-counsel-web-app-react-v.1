import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import { ReactComponent as PlayBtn } from "../../static/images/sidebar-icons/playbtn.svg"
import { ReactComponent as FullScreenBtn } from "../../static/images/sidebar-icons/fullscreen.svg"
import { ReactComponent as PauseBtn } from "../../static/images/sidebar-icons/pause.svg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Form, Input, InputNumber, Button, Card, Space, Select, message } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import Axios from 'axios';
import jwt from "jsonwebtoken";
import { Row, Col } from 'reactstrap';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import ReactPlayer from 'react-player';
import { findDOMNode } from 'react-dom'
import screenfull from 'screenfull'
import { Modal } from 'antd';
import { Link } from 'react-router-dom';


const { Meta } = Card;




function Addlaw(props) {
    const [title, settitle] = useState("")
    const [loading, setloading] = useState(false)
    const [body, setbody] = useState("")
    const [inputList, setInputList] = useState([{ Clause: "" }]);
    const [thefile, setfile] = useState("none")
    const [coverfile, setcoverfile] = useState("none")
    const [thumbfile, setthumbfile] = useState("none")
    const [profilephoto, setprofilephoto] = useState("")
    const [coverphoto, setcoverphoto] = useState("")
    const [tagname, settagname] = useState("")
    const [tags, settags] = useState([])
    const [images, setimages] = useState([])
    const [thumbnail, setthumbnail] = useState("")
    const [videoname, setvideoname] = useState("")
    const [videoid, setvideoid] = useState("")
    const [videourl, setvideourl] = useState("")
    const [playing, setplaying] = useState(true)
    const [theplayer, setplayer] = useState('')
    const [played, setplayed] = useState(0)
    const [seeking, setseeking] = useState(false)
    const [seeker, setseeker] = useState("none")
    const [mainbtn, setmainbtn] = useState("none")
    const [playerbtn, setplayerbtn] = useState("none")
    const [fullscrbtn, setfullscrbtn] = useState("none")
    const [Images, setImages] = useState([])
    const [selectedfile, setselectedfile] = useState("")
    const [newimages, setnewimages] = useState([])
    const [disp, setdisp] = useState("none")
    const [imgdisp, setimgdisp] = useState("none")
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [theradio, setradio] = useState("")
    const [thepublications, setpublications] = useState([])
    // const [tagsdup, settagsdup] = useState([])


    useEffect(() => {
        // console.log(props.getid)
        Axios.get('https://counsel.apimachine.com/api/getalltag')
            .then(response => {
                if (response.data.success) {
                    settags(response.data.data)
                } else {
                    message.error(response.data.message)
                    console.log(props.getlegid)
                }
            }).catch(err => console.log(err))
        Axios.get('https://counsel.apimachine.com/api/getpublications')
            .then(res => {
                if (res.data.success) {
                    setpublications(res.data.data)
                } else {
                    message.error(res.data.message)
                }
            })
    }, [])
    // tags.forEach(element => {
    //     tagsdup.push({ TagType: element.TagType, TagName: "" })
    // })
    const handleChange = (e) => {
        if (e.target.name === "title") {
            settitle(e.target.value)
        } else if (e.target.name === "body") {
            setbody(e.target.value)
        } else if (e.target.name === "videoname") {
            setvideoname(e.target.value)
        } else {
            message.error("error while adding input")
        }
    }
    const handleChangeEdit = (value) => {
        setbody(value)
    }
    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...inputList];
        list[index][name] = value;
        setInputList(list);
    };
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };
    const handleAddClick = () => {
        setInputList([...inputList, { Clause: "" }]);
    };
    let uploadimages = async (e) => {
        e.preventDefault()
        let selectedFiles = e.target.files
        // let Images = []
        // let filename
        // let selected_file
        console.log("files  " + selectedFiles.length)
        for (var i = 0; i < selectedFiles.length; i++) {
            const formData = new FormData
            const hide = message.loading("Uploading Images", 0)
            let fileName = selectedFiles[i].name
            console.log("name-of-file " + selectedFiles[i].name)
            let selected_file = selectedFiles[i]
            formData.append("files", selected_file)
            console.log("formdata " + (formData))
            const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf";
            const email = "ram@nvestbank.com";
            const path_inside_brain = "root/CounselAppImages/";
            // let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);

            const token = jwt.sign({ name: fileName, email: email }, secret, {
                algorithm: "HS512",
                expiresIn: 240,
                issuer: "gxjwtenchs512",
            });
            // console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
            let response = await Axios.post(
                `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                formData,
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );
            console.log(response.data)
            if (response.data.status) {
                images.push({ image: response.data.payload.url })
                setTimeout(hide, 100);
            } else {
                setTimeout(hide, 100);
                // message.error(response.data.payload)
                console.log("error")
            }
            console.log("Images " + JSON.stringify(images))
            // console.log("New ImaGE " + JSON.stringify(newimages))
        }
        if (selectedFiles.length === images.length) {
            message.success("Images uploaded")
            setdisp("block")
        } else {
            message.error("One or more images not uploaded, please try again")
        }
    }
    let viewimages = (e) => {
        e.preventDefault()
        setnewimages(images)
        setimgdisp("block")
    }
    let handleChange1 = async (e) => {
        e.preventDefault()
        if (e.target.files[0] === undefined) {
            message.error("Please upload files")
        } else {
            let filename = e.target.files[0].name
            console.log(e.target.files[0])
            console.log("name-file " + filename)
            let name = e.target.name
            console.log("name " + name)

            // let fileName = e.target.files[0].name
            let fileext = e.target.files[0].type
            let selected_file = e.target.files[0]
            console.log("file-type " + fileext)
            if (name === "profile" || name === "cover" || name === "thumbnail") {
                if (fileext.substring(0, 5) === "image") {
                    const formData = new FormData();
                    formData.append("files", selected_file)
                    const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                    const email = "ram@nvestbank.com"; //email of the developer.
                    const path_inside_brain = "root/CounselAppImages/";
                    let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                    console.log(fileName)
                    const token = jwt.sign({ name: fileName, email: email }, secret, {
                        algorithm: "HS512",
                        expiresIn: 240,
                        issuer: "gxjwtenchs512",
                    });

                    console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
                    let response = await Axios.post(
                        `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                        formData,
                        {
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                            },
                        }
                    );

                    console.log(JSON.stringify(response.data) + " image-upload");
                    console.log(selected_file.name + " imagename")

                    if (response.data.status) {
                        if (name === "profile") {
                            setfile("block")
                            setprofilephoto(response.data.payload.url)
                        } else if (name === "cover") {
                            setcoverfile("block")
                            setcoverphoto(response.data.payload.url)
                        } else if (name === "thumbnail") {
                            setthumbfile("block")
                            setthumbnail(response.data.payload.url)

                        }
                    } else {
                        message.error(response.data.payload)
                    }
                } else {
                    message.error("please upload only images")
                }
            }
        }
    }
    const videoupload = async (e) => {
        e.preventDefault()
        const hide = message.loading("Uploading Video", 0)
        let filename = e.target.files[0].name
        let selected_file = e.target.files[0]
        let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
        const formData = new FormData();
        formData.append("files", selected_file)

        const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
        const email = "ram@nvestbank.com"; //email of the developer.
        const path_inside_brain = "root/CounselAppImages/";

        const title1 = "Title of video";
        const subtitle = "Title of video";
        const video_description = "Description of the video";
        const thumbnailUrl = thumbnail //image url of the thumbail that is already uplaoded to brain.
        console.log(thumbnail + "url of thumbnail")

        const token = jwt.sign({ name: fileName, email: email }, secret, {
            algorithm: "HS512",
            expiresIn: 240,
            issuer: "gxjwtenchs512",
        });


        let response = await Axios.post(
            `https://drivetest.globalxchange.io/file/dev-upload-video?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}&title=${title1}&subtitle=${subtitle}&video_description=${video_description}&thumbnailUrl=${thumbnailUrl}`,
            formData,
            {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                },
            }
        );

        console.log(response.data);
        if (response.data.status) {
            setvideoid(response.data.payload.video_id)
            const secret1 = '9xr7w8KCuY2Wmhe89bR'; //secret not to be disclosed anywhere.
            const token1 = jwt.sign({}, secret1, {
                algorithm: "HS512",
                expiresIn: 240, //seconds
                issuer: "gxervench215",
            });
            let stream_link = await Axios.post(
                `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token1}`,
                {
                    "video_id": response.data.payload.video_id
                },
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );
            console.log("videolink " + JSON.stringify(stream_link));
            console.log("video-url " + stream_link.data)
            setTimeout(hide, 100);
            setmainbtn("block")
            setplayerbtn("block")
            setfullscrbtn("block")
            setseeker("block")
            setvideourl(stream_link.data)
            setimgdisp("none")
            console.log("video-length " + stream_link.content_length)
        } else {
            message.error(response.data.payload)
        }

    }
    const handleChangetags = (e, index) => {
        const { name, value } = e.target;
        const thelist = [...tags];
        thelist[index][name] = value;
        settags(thelist);
    }
    const thebtn = (e) => {
        console.log(e.target.src)
    }
    const onFinish = values => {
        console.log(values);
        console.log(title, body)
        console.log(JSON.stringify(inputList))
        console.log(tagname)

        let Tags = []
        for (let i = 0; i < tags.length; i++) {
            Tags.push({ Tag: [{ TagType: tags[i].TagType, TagName: tags[i].tagname }] })
        }
        // let Tags1 = []
        // for (let i = 0; i < tags.length; i++) {
        //     Tags1.push({ Tag: [{ TagType: tags[i].TagType, TagName: ""}] })
        // }
        // console.log(JSON.stringify(Tags))
        let body1 = {
            "Title": title,
            "Body": body,
            "Clauses": inputList,
            "Tags": Tags,
            "ProfileImage": profilephoto,
            "CoverPhoto": coverphoto,
            "VideoLink": videoid,
            "VideoName": videoname,
            "VideoThumbnail": thumbnail,
            "Images": newimages,
            "PublicationName": theradio
        }

        let array = []
        Tags.forEach(element => {
            if (element.Tag[0].TagName) {
                array.push("success")
            } else {
                array.push("failed")
            }
        })
        // Axios.post("https://counsel.apimachine.com/api/proposelaw/" + props.getlegid, body1).then(res => {
        //     if (res.data.success) {
        //         message.success("Law Created Successfully")
        //     } else {
        //         message.error(res.data.message)
        //     }
        // })
        let TagsUpt = []
        tags.forEach(element => {
            TagsUpt.push({ TagType: element.TagType, tagname: "" })
        })
        if (array.includes("failed")) {
            message.error("please enter all tagnames")
            console.log("input-list " + JSON.stringify(inputList))
            console.log("Tags " + JSON.stringify(tags))
        } else if (theradio === "") {
            message.error("Please select publication")
        } else {
            Axios.post("https://counsel.apimachine.com/api/proposelaw/" + props.getlegid, body1).then(res => {
                if (res.data.success) {
                    message.success("Law Created Successfully")
                    settitle("")
                    setbody("")
                    setfile("none")
                    setcoverfile("none")
                    setmainbtn("none")
                    setplayerbtn("none")
                    setfullscrbtn("none")
                    setseeker("none")
                    setthumbfile("none")
                    setradio("")
                    setInputList([{ Clause: "" }])
                    console.log("Tags " + JSON.stringify(tags))
                    settags(TagsUpt)
                } else {
                    message.error(res.data.message)
                }
            })
            // settitle("")
            // setbody("")
            // setfile("none")
            // setcoverfile("none")
            // setmainbtn("none")
            // setplayerbtn("none")
            // setfullscrbtn("none")
            // setseeker("none")
            // setthumbfile("none")
            // setInputList([{ Clause: "" }])
            // console.log("Tags " + JSON.stringify(tags))
            // settags(TagsUpt)
            // setimages()
            // setnewimages()
        }

        console.log("input-list " + JSON.stringify(inputList))
        // console.log("tags-upt " + JSON.stringify(tagsdup))
        console.log(JSON.stringify(body1))
    };
    // const onFinish1 = values => {
    //     console.log(values);
    // };
    const handlePause = () => {
        console.log('onPause')
        setplaying(false)
    }
    const handlePlay = () => {
        console.log('onPlay')
        setplaying(true)
    }
    const handlePlayPause = () => {
        setplaying(!playing)
    }
    const ref = player => {
        setplayer(player)
    }
    const handleClickFullscreen = () => {
        screenfull.request(findDOMNode(theplayer))
    }
    const handleDuration = (duration) => {
        console.log('onDuration', duration)
        // setduration(duration)
    }
    const handleSeekChange = (e) => {
        setplayed(parseFloat(e.target.value))
    }

    const handleSeekMouseDown = e => {
        setseeking(true)
    }
    const handleSeekMouseUp = e => {
        theplayer.seekTo(parseFloat(e.target.value))
    }
    const handleProgress = state => {
        console.log('onProgress', state)
        setplayed(state.played)
    }

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        console.log(theradio + " publication")
        setIsModalVisible(false);
    };
    const selectpublication = (e) => {
        e.preventDefault()
        console.log(e.target.value + " select")
    }
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Propose law</span>
        </div>
        {/* <p>{props.getlegid}</p> */}


        <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            <Form.List name="users">
                {(fields, { add, remove }) => {
                    return (
                        <div>
                            <div className="crd-style">
                                <Button type="primary" onClick={showModal}>
                                    Select Publication
                                    </Button>
                                {/* onClick = {(e)=>setradio(element.PublicationName)} */}
                                <Modal title="Publication" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={700}>
                                    <b>{theradio}</b>
                                    <Row>
                                        {thepublications.map(element => {
                                            return <Col sm={4}>
                                                <div class="card thecard" onClick={(e) => setradio(element.PublicationName)}>
                                                    <img src={element.Icon} className="theimgs-style" />
                                                    <div class="container">
                                                        <h4><b>{element.PublicationName}</b></h4>
                                                        <a href={element.PublicationLink} target="_blank">{element.PublicationLink}</a>
                                                    </div>
                                                </div>
                                            </Col>
                                        })}
                                    </Row>
                                </Modal>
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Publication Name</p>
                                <p className="inputtitle">{theradio}</p>
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Title</p>
                                <input type="text" name="title" className="inputtitle" placeholder="Ex: law firm" value={title} onChange={handleChange} required />
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Body</p>
                                <ReactQuill style={{ marginLeft: "25px", border: "none", width: "480px" }} rows="4" cols="50" value={body} onChange={handleChangeEdit} required />
                                {/* <textarea style={{ marginLeft: "25px", border: "none", width: "480px" }} placeholder="Enter description..." className="inputtxtarea" rows="4" cols="50" value={body} name="body" onChange={handleChange} required></textarea> */}
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Upload Profile Photo</p><br />
                                <div style={{ display: "flex" }}>
                                    <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="profile" onChange={handleChange1} required />
                                    <img src={profilephoto} style={{ display: thefile }} className=" img-stye" alt="profilephoto" />
                                </div>
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Upload Cover Photo</p><br />
                                <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="cover" onChange={handleChange1} required />
                                <img src={coverphoto} style={{ display: coverfile }} className="img-stye1" alt="coverphoto" />
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Clauses</p>
                                {inputList.map((x, i) => {
                                    return (
                                        <div>
                                            <div style={{ display: "flex" }}>
                                                <p className="theclausesty">Clause</p>
                                                <input
                                                    name="Clause"
                                                    placeholder="Clause"
                                                    className="theinpclausesty"
                                                    value={x.Clause}
                                                    onChange={e => handleInputChange(e, i)}
                                                />
                                                {inputList.length !== 1 && <MinusCircleOutlined
                                                    className="mr10 theminusbtn"
                                                    onClick={() => handleRemoveClick(i)} />}
                                                <div>
                                                </div>
                                            </div>
                                            {inputList.length - 1 === i && <Button className="addclausebtn" type="dashed" onClick={handleAddClick}><PlusOutlined /> Add Clause</Button>}
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Tags</p><br />
                                {tags.map((x, i) => {
                                    return <div>
                                        <Row>
                                            <Col sm="3">
                                                <p className="theclausesty" name="tagname" value="tagname">{x.TagType.charAt(0).toUpperCase()}{x.TagType.slice(1)}</p>
                                            </Col>
                                            <Col sm="1"></Col>
                                            <Col sm="8">
                                                <input
                                                    name="tagname"
                                                    placeholder="TagName"
                                                    className="theinpclausesty"
                                                    value={x.tagname}
                                                    onChange={e => handleChangetags(e, i)}
                                                />
                                            </Col>
                                        </Row>
                                    </div>
                                })}
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Upload Images</p><br />
                                <input type="file" className="theimage-styy" name="cover" multiple onChange={uploadimages} />
                                <button style={{ display: disp }} className="previewbtn" onClick={viewimages}>Preview</button>
                                {newimages.map(element => {
                                    return <div style={{ display: "flex" }}><img style={{ display: imgdisp }} src={element.image} className="img-stye2" alt="coverphoto" /></div>
                                })}
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Video Name</p>
                                <input type="text" name="videoname" className="inputtitle" placeholder="Enter name of the video" value={videoname} onChange={handleChange} />
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Video Thumbnail</p><br />
                                <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="thumbnail" onChange={handleChange1} />
                                <img src={thumbnail} style={{ display: thumbfile }} className="img-stye1" alt="coverphoto" />
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Upload Video:</p><br />
                                <input type="file" style={{ color: "transparent" }} className="theimage-styy" name="video" onChange={videoupload} />
                                <ReactPlayer
                                    style={{ display: mainbtn }}
                                    ref={ref}
                                    url={videourl}
                                    light={thumbnail}
                                    controls={false}
                                    className="theplayer-sty"
                                    width="100%"
                                    height="310px"
                                    onPause={handlePause}
                                    playing={playing}
                                    onPlay={handlePlay}
                                    pip={false}
                                    onSeek={e => console.log('onSeek', e)}
                                    onProgress={handleProgress}
                                    onDuration={handleDuration}
                                />
                                <div >
                                    <p onClick={handlePlayPause} style={{ display: playerbtn }} className="theplaypausebtn-sty">{playing ? <PauseBtn /> : <PlayBtn />}</p>
                                    <p onClick={handleClickFullscreen} style={{ display: fullscrbtn }} className="thefullscrn-btn "><FullScreenBtn /></p>
                                </div>
                                <input
                                    type='range' min={0} max={0.999999} step='any'
                                    className="theplayerseek"
                                    value={played}
                                    style={{ display: seeker }}
                                    onMouseDown={handleSeekMouseDown}
                                    onChange={handleSeekChange}
                                    onMouseUp={handleSeekMouseUp}
                                />
                                {/* <video  width="320" height="240" controls src={videoname} style={{ display: videofile }} className="img-stye1" alt="coverphoto" ></video> */}
                            </div>
                        </div>
                    );
                }}
            </Form.List>

            <button type="submit" style={{ position: "initial" }}
                className="footer btmste"
                disabled={loading}
            >
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
            </button>
        </Form>
    </div>;
}

export default Addlaw;
