import React, { useState } from 'react';
import whitearrow from "../../static/images/sidebar-icons/logo/arrow-white.png"
import OtpInput from 'react-otp-input';
import axios from "axios";
import message from 'antd/lib/message';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

function EmailPage(props) {
    const [code1, setcode1] = useState("")
    const [loading, setloading] = useState(false)

    const handleChange = (e) => {
        setcode1(e)
        console.log("thecode" + code1)
    }
    const resendclick = (e) => {
        e.preventDefault()
        let body1 = {
            "email":  props.getemail[0]
        }
        axios.post("https://gxauth.apimachine.com/gx/user/confirm/resend", body1).then(res=>{
            console.log(res.data)
            if(res.data.status){
                message.success(res.data.message)
            }else{
                message.error(res.data.message)
            }
        })
    }
    const handleSubmit = (e) => {
        setloading(true)
        e.preventDefault()
        let body = {
            "email":  props.getemail[0],
            "code": code1.toString()
        }
        axios.post("https://gxauth.apimachine.com/gx/user/confirm", body).then(res => {
            console.log(res.data)
            if (res.data.status) {
                let thebody
                if (props.getemail[1] === "User") {
                    thebody = {
                        "email": props.getemail[0],
                        "app_code": "counsel"
                    }
                } else {
                    thebody = {
                        "email": props.getemail[0],
                        "app_code": "counsel",
                        "userType": props.getemail[1]
                    }
                }
                console.log("user-reg " + JSON.stringify(thebody))
                axios.post('https://counsel.apimachine.com/api/register/user', thebody).then(res1 => {
                    console.log(res1.data)
                    if (res1.data.success) {
                        setloading(false)
                        message.success(res1.data.message)
                        props.gotomain()
                        props.sendtomain(props.getemail[1])
                    } else {
                        setloading(false)
                        message.error(res1.data.message)
                    }
                }).catch(err => console.log(err))
            } else {
                setloading(false)
                message.error(res.data.message)
            }
        }).catch(err => console.log(err))
        console.log("confirm-mail " + JSON.stringify(body))
    }
    // const testone = (e) => {
    //     e.preventDefault()
    // }
    return <div className="outer">
        <div className="middle">
            <div className="inner">
                <h4 className="usrnm-sty">Email Confirmation</h4>
                <p className="gxtitle-sty"> Enter The Code That Was Just Sent To {props.getemail[0]}</p>
                <form onSubmit={handleSubmit}>
                    <div>
                        <OtpInput
                            onChange={handleChange}
                            numInputs={6}
                            inputStyle={{ width: "30px", border: "none", borderBottom: "1px solid #443380", textAlign: "center", color: "#334480" }}
                            value={code1}
                            separator={<span>&nbsp;&nbsp;</span>}
                            isInputNum={true}
                        />
                    </div><br />
                    {/* <p onClick = {testone}>test button</p> */}
                    <div>
                        <p className = "resend-sty">Didn't receive any code ? Allow us to <button className = "btnresnd-sty" onClick = {resendclick}>Resend It</button></p>
                        <img src={whitearrow} alt="backarrow" className="whitearrow-sty" onClick={() => props.onClose()} />
                        <button type="submit" className="centerbtn-sty" disabled={loading}>{loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Continue"}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
}

export default EmailPage;
