import React, { useEffect, useState } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios";

function AddFullUserProfile(props) {
    const [profiledata, setprofiledata] = useState([])
    let thedisplay
    if (props.gettype === "User") {
        thedisplay = "Corporate"
    } else if (props.gettype === "Legal Professional") {
        thedisplay = "Legal"
    }

    useEffect(() => {
        setprofiledata([])
        axios.get('https://counsel.apimachine.com/api/getuserdetailsbyidwithprofile/' + props.getobjid)
            .then(res => {
                setprofiledata(res.data.data)
            }).catch(err => console.log(err))
    }, [props.getobjid])

    const clicklawfirm = (e)=>{
        e.preventDefault()
        console.log(e.target.name + "thelEGal-ID")
    }
    console.log(JSON.stringify(profiledata[0]) + " theprofile")
    if (profiledata[0] !== undefined && props.gettype === "User") {
        return <div>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
  &nbsp;&nbsp;&nbsp;<span className="add-style">Corporate Profiles</span>
            </div>
            {profiledata[0].CorporateProfile.map(element => {
                return <div className="crd-style styll">
                    <p className="body-sty sty1 fonts1">Corporate ID&nbsp;:&nbsp;<span className="profile-sty">{element.CorporateID}</span></p>
                    <p className="body-sty fonts1">Corporate Name&nbsp;:&nbsp;<span className="profile-sty">{element.CorporateName}</span></p>
                </div>
            })}
        </div>
    } else if (profiledata[0] !== undefined && props.gettype === "Legal Professional") {
        return <div>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
  &nbsp;&nbsp;&nbsp;<span className="add-style">Legal Profiles</span>
            </div>
            {profiledata[0].LegalProfile.map(element => {
                console.log(element.LawFirmAdministration + "asdfg")
                let required
                let required1
                let required2
                let required3
                let required4
                let required5
                let required6
                if (element.LawFirm === undefined) {
                    required = "none"
                }
                if (element.LawFirmConsultant === undefined) {
                    required1 = "none"
                }
                if (element.LawFirmAdministration === undefined) {
                    required2 = "none"
                }
                if (element.Arbitrators === undefined) {
                    required3 = "none"
                }
                if (element.Judges === undefined) {
                    required4 = "none"
                }
                if (element.GeneralCounsels === undefined) {
                    required5 = "none"
                }
                if (element.Lawyers === undefined) {
                    required6 = "none"
                }
                return <div className="crd-style styll">
                    {/* <p>{element}</p> */}
                    <p className="body-sty sty1 fonts1">Legal ID&nbsp;:&nbsp;<span className="profile-sty">{element.LegalID}</span></p>
                    <p className="body-sty fonts1" style={{ display: required }}>Law Firm&nbsp;:&nbsp;<span className="profile-sty">{element.LawFirm}</span></p>
                    <div className="btn-styy">
                        <button type="button" style={{ display: required }} className="button-sty1" onClick={() => {props.openaddfirm(); props.sendLawID(element.LegalID)}}>Add Law Firm Profile</button>
                        <button type="button" style={{ display: required }} className="button-sty1" onClick = {()=> {props.openlawfirm(); props.sendLegalID1(element.LegalID)}}>View Law Firm Profile</button>
                        {/* <button type="button" style={{ display: required }} className="button-sty1" >Approved Laws</button> */}
                    </div>
                    <p className="body-sty fonts1" style={{ display: required1 }}>Law Firm Consultant&nbsp;:&nbsp;<span className="profile-sty">{element.LawFirmConsultant}</span></p>
                    <p className="body-sty fonts1" style={{ display: required2 }}>Law Firm Administration&nbsp;:&nbsp;<span className="profile-sty">{element.LawFirmAdministration}</span></p>
                    <p className="body-sty fonts1" style={{ display: required3 }}>Arbitrators&nbsp;:&nbsp;<span className="profile-sty">{element.Arbitrators}</span></p>
                    <p className="body-sty fonts1" style={{ display: required4 }}>Judges&nbsp;:&nbsp;<span className="profile-sty">{element.Judges}</span></p>
                    <p className="body-sty fonts1" style={{ display: required5 }}>GeneralCounsels&nbsp;:&nbsp;<span className="profile-sty">{element.GeneralCounsels}</span></p>
                    <p className="body-sty fonts1" style={{ display: required6 }}>Lawyers&nbsp;:&nbsp;<span className="profile-sty">{element.Lawyers}</span></p>                
                </div>
            })}
        </div>
    } else {
        return <div>Loading.....</div>
    }
}

export default AddFullUserProfile;
