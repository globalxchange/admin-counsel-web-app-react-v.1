import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import "./pages.scss";
import axios from "axios";
import message from 'antd/lib/message';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';


function WorkSpaceTwo(props) {
    const [email1, setemail1] = useState('')
    const [loading, setloading] = useState(false)
    const [code,setcode] = useState('')

let handleChange1 = (e) => {
    setcode(e)
    console.log("code " + code )
}
    let handleChange = (e) => {
        e.preventDefault()
        setemail1(e.target.value)
        console.log(email1)
    }
    let handleSubmit = (e) => {
        e.preventDefault()
        setloading(true)
        axios.get('https://comms.globalxchange.io/user/details/get?email=' + email1)
            .then(response => {
                if (response.data.status) {
                    let body
                    if (props.type === "User") {
                        body = {
                            "email": email1,
                            "app_code": "counsel"
                        }
                    } else {
                        body = {
                            "email": email1,
                            "app_code": "counsel",
                            "userType": props.type
                        }
                    }
                    axios.post('https://counsel.apimachine.com/api/register/user', body).then(res => {
                        console.log(res.data)
                        setloading(false)
                        if (res.data.success) {
                            message.success(res.data.message)
                            setemail1('')
                            props.openmain()
                            props.sendmain(props.type)
                        } else {
                            message.error(res.data.message)
                        }
                    }).catch(err => console.log(err))
                    console.log("the-body" + JSON.stringify(body))
                } else {
                    setloading(false)
                    props.onCloseAddUser()
                    props.sendEmail([email1, props.type])
                }
            }).catch(err => console.log(err))
    }

    let thevalue = "Add " + props.type
    return (
        <div>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add {props.type}</span>
            </div>
            <form onSubmit={handleSubmit}>
                <div className="crd-style">
                    <p className="adjust-title">What Is Your Email ID ?</p>
                    <input type="email" name="email" placeholder="Ex: shorupan@nvestbank.com" value={email1} onChange={handleChange} required />
                </div>
                <button type="submit"
                    className="footer"
                    disabled={loading}
                >
                    {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : thevalue}
                </button>
            </form>
        </div>
    )
}

export default WorkSpaceTwo