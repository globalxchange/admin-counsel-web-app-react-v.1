import React, { useState, useContext } from 'react';
import Sider from 'antd/lib/layout/Sider';
import { ReactComponent as IconPlus } from '../../static/images/sidebar-icons/plus.svg';
import { ReactComponent as IconOver } from '../../static/images/sidebar-icons/dash.svg';
import { ReactComponent as RedCircle } from '../../static/images/sidebar-icons/redcircle.svg';
import { Drawer } from 'antd';
import msgIcon from '../../static/images/sidebar-icons/msg-icon.svg';
import logo from '../../static/images/sidebar-icons/logo/logo.svg';
import logoFull from '../../static/images/sidebar-icons/logo/logo-full.svg';
import { CounselAppContext } from '../../Context_Api/Context';
import { Link } from 'react-router-dom';
import WorkSpaceOne from "../../pages/Home/WorkSpaceOne";
import WorkSpaceTwo from "../../pages/Home/WorkSpaceTwo";
import WorkSpaceThree from "../../pages/Home/WorkSpaceThree";
import AllUsersByType from "../../pages/Home/AllUsersByType";
import UserRegisterName from "../../pages/Home/UserRegisterName";
import PasswordPage from "../../pages/Home/PasswordPage";
import CnfrmPassword from "../../pages/Home/CnfrmPassword";
import EmailPage from "../../pages/Home/EmailPage";
import AddProfile from "../../pages/Home/AddProfile";
import FullProfile from "../../pages/Home/FullProfile";
import AddUserProfile from "../../pages/Home/AddUserProfile";
import AddFullUserProfile from "../../pages/Home/AddFullUserProfile";
import Addlaw from "../../pages/Home/Addlaw";
import ProposedLaw from "../../pages/Home/proposedlaws";
import ApprovedLaw from "../../pages/Home/ApproveLaw";
import AddLawProfile from "../../pages/Home/AddLawProfile";
import ViewLawFirm from "../../pages/Home/ViewLawFirm";
import AddSpecializations from "../../pages/Home/AddSpecializations";
import EditLawArticles from "../../pages/Home/editlawarticle"

function Sidebar({ active }) {
  const { login, profilePic, profileName } = useContext(CounselAppContext);
  const [collapsed, setCollapsed] = useState(false);
  const [visible, setVisible] = useState(false);
  const [visible2, setVisible2] = useState(false);
  const [visible3, setVisible3] = useState(false);
  const [visible4, setVisible4] = useState(false);
  const [visible5, setVisible5] = useState(false);
  const [visible6, setVisible6] = useState(false);
  const [visible7, setVisible7] = useState(false);
  const [visible8, setVisible8] = useState(false);
  const [visible9, setVisible9] = useState(false);
  const [visible10, setVisible10] = useState(false);
  const [visible11, setVisible11] = useState(false);
  const [visible12, setVisible12] = useState(false); 
  const [visible13, setVisible13] = useState(false);
  const [visible14, setVisible14] = useState(false);
  const [visible15, setVisible15] = useState(false);
  const [visible16, setVisible16] = useState(false);
  const [visible17, setVisible17] = useState(false);
  const [visible18, setVisible18] = useState(false);
  const [visible19, setVisible19] = useState(false);
  const [variable, setvariable] = useState("");
  const [lawfirm, setlawfirm] = useState("")
  const [objid, setobjid] = useState("")
  const [variable1, setvariable1] = useState("");
  const [data12, setdata12] = useState("");
  const [theid, settheid] = useState("")
  const [viewid, setviewid] = useState("")
  const [usertype, setusertype] = useState("")
  const [lawid, setlawid] = useState("")
  const [viewlegalid, setviewlegalid] = useState("")
  const [getemail, setgetemail] = useState([]);
  const [name, setname] = useState([])
  const [email, setemail] = useState([])
  const [data, setdata] = useState([])
  const [legalid,setlegalid] = useState("")
  const [title, settitle] = useState("")
  const [body, setbody] = useState("")
  const [image, setimage] = useState("")
  const [photo, setphoto] = useState("")
  const [data11, setdata11] = useState("")
  const [videoname, setvideoname] = useState("")
  const [thumbnail, setvideothumbnail] = useState("")
  const [thelink, setvideolink] = useState("")
  const [allimages, settheimages] = useState([])
  // const [editarticle,seteditarticle] = useState("")
  const onCollapse = (collapsed) => {
    console.log(collapsed);
    setCollapsed(collapsed);
  };

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = async () => {
    setVisible(false)
  }
  const onClose2 = () => {
    setVisible(false)
    setVisible2(true)
  }
  const onClose3 = () => {
    setVisible(false)
    setVisible2(false)
    setVisible3(false)
    setVisible4(false)
    setVisible5(false)
    setVisible6(false)
    setVisible7(false)
    setVisible8(false)
    setVisible9(false)
  }
  const onClose11 = () => {
    setVisible(true)
    setVisible2(false)
    setVisible3(false)
  }
  const onClose13 = () => {
    setVisible(false)
    setVisible4(true)
  }
  const CloseAllUser = () => {
    setVisible(true)
    setVisible4(false)
  }
  const onClose4 = () => {
    setVisible(false)
    setVisible2(false)
    setVisible3(true)
  }
  const onCloseAddUser = () => {
    setVisible2(false)
    setVisible5(true)
  }
  const onCloseBack = () => {
    setVisible5(false)
    setVisible2(true)
  }
  const PasswordPage1 = () => {
    setVisible5(false)
    setVisible6(true)
  }
  const CnfrmPasswordPage = () => {
    setVisible6(false)
    setVisible7(true)
  }
  const onClosePasswordPage = () => {
    setVisible6(false)
    setVisible5(true)
  }
  const onCloseCnfrmPassword = () => {
    setVisible7(false)
    setVisible6(true)
  }
  const handlemailpage = () => {
    setVisible7(false)
    setVisible8(true)
  }
  const closemailpage = () => {
    setVisible7(true)
    setVisible8(false)
  }
  const handlemain = () => {
    setVisible2(false)
    setVisible(true)
  }
  const handlethemain = () => {
    setVisible8(false)
    setVisible(true)
  }
  const addprofile = () => {
    setVisible4(false)
    setVisible9(true)
  }
  const addtheprofiles = () => {
    setVisible4(false)
    setVisible11(true)
  }
  const viewprofile = () => {
    setVisible4(false)
    setVisible10(true)
  }
  const closeviewprofile = () => {
    setVisible4(true)
    setVisible10(false)
  }
  const closeuserprofile = () => {
    setVisible4(true)
    setVisible11(false)
  }
  const theprofile = () => {
    setVisible4(false)
    setVisible12(true)
  }
  const closefullprofile = () => {
    setVisible4(true)
    setVisible12(false)
  }
  const closeaddprofile = () => {
    setVisible4(true)
    setVisible9(false)
  }
  const addlaw = () => {
    setVisible4(false)
    setVisible13(true)
  }
  const closelaw = ()=>{
    setVisible4(true)
    setVisible13(false)
  }
  const openproposelaw = ()=>{
    setVisible4(false)
    setVisible14(true)
  }
  const closeproposedlaw = ()=>{
    setVisible4(true)
    setVisible14(false)
  }
  const approvedlaw = ()=>{
    setVisible4(false)
    setVisible15(true)
  }
  const closeapprovelaw = ()=>{
    setVisible4(true)
    setVisible15(false)
  }
  const openaddfirm = ()=>{
    setVisible12(false)
    setVisible16(true)
  }
  const openlawfirm = ()=>{
    setVisible12(false)
    setVisible17(true)
  }
  const closeaddfirm = ()=>{
    setVisible16(false)
    setVisible12(true)
  }
  const closeviewfirm = ()=>{
    setVisible17(false)
    setVisible12(true)
  }
  const openspecialization = ()=>{
    setVisible17(false)
    setVisible18(true)
  }
  const closespecialization = ()=>{
    setVisible17(true)
    setVisible18(false)
  }
  const closecard = ()=>{
    setVisible19(false)
    setVisible14(true)
  }
  const datatomain = (v) => {
    setdata12(v)
  }
  const sendmaindata = (v) => {
    setdata12(v)
  }
  const handletext = (v) => {
    console.log(v + " the-var")
    setvariable(v)
  }
  const handletext1 = (v) => {
    console.log(v + "the-var2")
    setvariable1(v)
  }
  const handleName = (v) => {
    setname(v)
  }
  const handleEmail = (v) => {
    setemail(v)
  }
  const handleData = (v) => {
    setdata(v)
  }
  const handlegetemail = (v) => {
    setgetemail(v)
  }
  const setid = (v) => {
    settheid(v)
  }
  const sendid = (v) => {
    setviewid(v)
  }
  const getkey = (v) => {
    setusertype(v)
  }
  const theobjid = (v) => {
    setobjid(v)
  }
  const theLawID = (v)=>{
    setlawid(v)
  }
  const sendtheid = (v)=>{
    setviewlegalid(v)
  }
  const thelawfirmid = (v)=>{
    setlawfirm(v)
  }
  const editarticle = ()=>{
    setVisible14(false)
    setVisible19(true)
  }
  const theeditid = (v)=>{
    setlegalid(v)
  }
  const thetitle = (v)=>{
    settitle(v)
  }
  const thebody = (v)=>{
    setbody(v)
  }
  const thephoto = (v)=>{
    setphoto(v)
  }
  const theprofileimage = (v)=>{
    setimage(v)
  }
  const thevideoname = (v)=>{
    setvideoname(v)
  }
  const thevideothumbnail = (v)=>{
    setvideothumbnail(v)
  }
  const thevideolink = (v)=>{
    setvideolink(v)
  }
  const theimages = (v)=>{
    settheimages(v)
  }
  const refresh = (v)=>{
    setdata11(v)
  }
  return (
    <Sider
      className="bets-dash-sider"
      theme="light"
      collapsible
      collapsed={collapsed}
      onCollapse={onCollapse}
      width={300}
      trigger={null}>
      <div
        className={'logo ' + collapsed}
        onClick={() => setCollapsed(!collapsed)}
      >
        {collapsed ? <img src={logo} alt="" /> : <img src={logoFull} alt="" />}
      </div>
      <div className={'sider-contents ' + collapsed}>
        <div className="group">
          <Link
            to="/customer-relations"
            className={
              'menu-itm' + (active === 'customer-relations' ? ' active' : '')
            }
          >
            {collapsed ? <IconOver className={"icon-sty" + (active === 'customer-relations' ? ' active' : '')} /> : ''} <h4>Customer Relations</h4>
          </Link>
          <div className="custrel-sty">
            <Link
              to="/traction"
              className={
                'menu-itm' + (active === 'traction' ? ' active' : '')
              }
            >
              <IconOver /> <h5>Traction</h5>
            </Link>
            <Link
              to="/marketing"
              className={
                'menu-itm' + (active === 'marketing' ? ' active' : '')
              }
            >
              <IconOver /> <h5>Marketing</h5>
            </Link>
            <Link
              to="/CRM"
              className={
                'menu-itm' + (active === 'CRM' ? ' active' : '')
              }>
              <IconOver /> <h5>CRM</h5>
            </Link>
          </div>
          <div className="menu-itm" onClick={showDrawer} >
            <IconPlus /> <h5>Add New</h5>
          </div>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
            maskStyle={{ opacity: "0" }}
            bodyStyle={{ marginBottom: "0px" }}
          >
            < WorkSpaceOne setVariable1={handletext} setVariable2={handletext1} onClose={onClose2} onClose12={onClose4} onClose13={onClose13} onCloseall={onClose3} getthedata={data12} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible2}
            maskStyle={{ opacity: "0" }}
          >
            < WorkSpaceTwo onClose11={onClose11} onCloseAddUser={onCloseAddUser} type={variable} sendEmail={handleEmail} openmain={handlemain} sendmain={sendmaindata} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={onClose3}
            visible={visible3}
            maskStyle={{ opacity: "0" }}
          >
            <WorkSpaceThree onClose11={onClose11} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={onClose3}
            visible={visible4}
            maskStyle={{ opacity: "0" }}
          >
            <AllUsersByType type={variable1} getproposedlaw = {openproposelaw} onClose11={CloseAllUser} addlaw = {addlaw}  openfullprofile = {theprofile} sendobjId = {theobjid} addprofile={addprofile} getapprovedlaw = {approvedlaw} addtheprofiles={addtheprofiles} setId={setid} viewprofile={viewprofile} thekey = {getkey} sendId={sendid} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible5}
            maskStyle={{ opacity: "0" }}
          >
            <UserRegisterName onClose={onCloseBack} PasswordPage={PasswordPage1} sendName={handleName} email={email} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            // visible={true}
            visible={visible6}
            maskStyle={{ opacity: "0" }}
          >
            <PasswordPage CnfrmPasswordPage={CnfrmPasswordPage} onClose={onClosePasswordPage} name={name} sendData={handleData} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible7}
            maskStyle={{ opacity: "0" }}
          >
            <CnfrmPassword onClose={onCloseCnfrmPassword} theformdata={data} emailpage={handlemailpage} sendEmail={handlegetemail} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible8}
            maskStyle={{ opacity: "0" }}
          >
            <EmailPage getemail={getemail} onClose={closemailpage} gotomain={handlethemain} sendtomain={datatomain} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible11}
            maskStyle={{ opacity: "0" }}
          >
            <AddUserProfile onClose={closeuserprofile} setId = {theid} getkey = {usertype}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible12}
            maskStyle={{ opacity: "0" }}
          >
            <AddFullUserProfile getobjid = {objid} gettype = {usertype} onClose = {closefullprofile} sendLawID = {theLawID} openlawfirm = {openlawfirm} openaddfirm = {openaddfirm} sendLegalID1={sendtheid}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible9}
            maskStyle={{ opacity: "0" }}
          >
            <AddProfile getid={theid} onClose={closeaddprofile} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible10}
            maskStyle={{ opacity: "0" }}
          >
            <FullProfile profileid={viewid} onClose={closeviewprofile} />
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            // visible = {visible13}
            visible={visible13}
            maskStyle={{ opacity: "0" }}
          >
            <Addlaw onClose = {closelaw} getlegid = {objid}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible14}
            // visible={true}
            maskStyle={{ opacity: "0" }}
          >
            <ProposedLaw onClose = {closeproposedlaw} 
            getlegid = {objid} 
            editlawarticle = {editarticle} 
            sendid = {theeditid} 
            sendtitle = {thetitle} 
            sendbody = {thebody} 
            sendcoverphoto = {thephoto} 
            sendprofileimage = {theprofileimage}
            sendvideoname = {thevideoname}
            sendvideothumbnail = {thevideothumbnail}
            sendvideolink = {thevideolink} 
            sendimages = {theimages}
            thedata = {data11}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible15}
            maskStyle={{ opacity: "0" }}
          >
           <ApprovedLaw onClose = {closeapprovelaw} getlegid = {objid}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible16}
            maskStyle={{ opacity: "0" }}
          >
            <AddLawProfile onClose = {closeaddfirm} lawid = {lawid}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible17}
            maskStyle={{ opacity: "0" }}
          >
            <ViewLawFirm gettheid = {viewlegalid} openspecialization = {openspecialization} onClose = {closeviewfirm} sendlawprofileid = {thelawfirmid}/>
          </Drawer>
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible18}
            maskStyle={{ opacity: "0" }}
          >
            <AddSpecializations onClose = {closespecialization} lawfirm = {lawfirm}/>
          </Drawer>
          {/* <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible18}
            maskStyle={{ opacity: "0" }}
          >
            <AddSpecializations onClose = {closespecialization} lawfirm = {lawfirm}/>
          </Drawer> */}
          <Drawer
            width={560}
            placement="right"
            closable={false}
            onClose={false}
            visible={visible19}
            // visible = {true}
            maskStyle={{ opacity: "0" }}
          >
            <EditLawArticles 
            getid = {legalid} 
            send = {refresh} 
            lawfirm = {lawfirm} 
            title = {title} 
            body = {body} 
            photo = {photo} 
            image = {image} 
            videoname = {videoname}
            thumbnail = {thumbnail}
            thelink = {thelink}
            allimages = {allimages}
            onClose = {closecard}/>
          </Drawer>
        </div>
        <div className="group">
          <Link
            to="/management"
            className={
              'menu-itm' + (active === 'management' ? ' active' : '')
            }
          >
            {collapsed ? <IconOver className={"icon-sty" + (active === 'management' ? ' active' : '')} /> : ''} <h4>Management</h4>
          </Link>
          <Link
            to="/employees"
            className={'menu-itm' + (active === 'employees' ? ' active' : '')}
          >
            <IconOver /> <h5>Employees</h5>
          </Link>
          <Link
            to="/ITndData"
            className={'menu-itm' + (active === 'ITndData' ? ' active' : '')}
          >
            <IconOver /> <h5>IT & data</h5>
          </Link>
          <Link
            to="/Admin"
            className={'menu-itm' + (active === 'Admin' ? ' active' : '')}
          >
            <IconOver /> <h5>Admin</h5>
          </Link>
        </div>
        <div className="group">
          <Link
            to="/applications"
            className={
              'menu-itm' + (active === 'addons' ? ' active' : '')
            }
          >
            {collapsed ? <RedCircle className={"icon-sty" + (active === 'addons' ? ' active' : '')} /> : ''} <h4>Applications</h4>
          </Link><h5 className="size-adj">Explore More</h5>
          <Link
            to="/cryptolaw"
            className={'menu-itm' + (active === 'cryptolaw' ? ' active' : '')}
          >
            <RedCircle /> <h5>Crypto Law</h5>
          </Link>
          <Link
            to="/mycounsel"
            className={'menu-itm' + (active === 'mycounsel' ? ' active' : '')}
          >
            <RedCircle /> <h5>My Counsel</h5>
          </Link>
          <Link
            to="/procounsel"
            className={'menu-itm' + (active === 'procounsel' ? ' active' : '')}
          >
            <RedCircle /> <h5>Pro Counsel</h5>
          </Link>
          <Link
            to="/regulator"
            className={'menu-itm' + (active === 'regulator' ? ' active' : '')}
          >
            <RedCircle /> <h5>Regulator</h5>
          </Link>
          <Link
            to="/regulator"
            className={'menu-itm' + (active === 'regulator' ? ' active' : '')}
          >
            <RedCircle /> <h5>Sample</h5>
          </Link>
        </div>
        <div className="profile-logout">
          <img className="dp" src={profilePic} alt="" />
          <div className="name px-2">
            <div className="">
              <h4>{profileName}</h4>
              <Link
                to="/login"
                onClick={() => {
                  login();
                }}
              >
                Log out
              </Link>
            </div>
            <div className="msg">
              <div className="count">0</div>
              <img src={msgIcon} alt="" />
            </div>
          </div>
        </div>
      </div>
    </Sider>
  );
}

export default Sidebar;
