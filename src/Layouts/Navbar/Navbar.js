import React, { useState, useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Menu, Dropdown } from 'antd';
import { faPlus, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faBell } from '@fortawesome/free-regular-svg-icons';
import { ReactComponent as IconBoards } from '../../static/images/navbar-icons/boards.svg';
import { ReactComponent as IconList } from '../../static/images/navbar-icons/list.svg';
import { CounselAppContext } from '../../Context_Api/Context';
function Navbar({ active }) {
  const [endselected, setEndselected] = useState('Toggle 1');
  const [tabSelected, setTabSelected] = useState('sub1');
  const { profilePic } = useContext(CounselAppContext);

  const getName = (active) => {
    switch (active) {
      case 'customer-relations':{
        return 'Customer Relations'
      }
      case 'traction': {
        return 'Traction';
      }
      case 'marketing': {
        return 'Marketing';
      }
      case 'CRM': {
        return 'CRM';
      }    
      case 'management':{
        return 'Management'
      }  
      case 'employees': {
        return 'Employees';
      }
      case 'ITndData': {
        return 'IT & Data';
      }
      case 'Admin':{
        return 'Admin'
      }
      case 'myapps1': {
        return 'Myapps 1';
      }
      case 'addons':{
        return 'Applications'
      }
      case 'cryptolaw':{
        return 'Crypto Law'
      }
      case 'mycounsel':{
        return 'My Counsel'
      }
      case 'procounsel':{
        return 'Pro Counsel'
      }
      case 'regulator':{
        return 'Regulator'
      }
      default: {
        return 'Name';
      }
    }
  };

  const menu = (
    <Menu className="drop-down-menu">
      <Menu.Item
        onClick={() => {
          setEndselected('Toggle 1');
          setTabSelected('Overview');
        }}
      >
        Toggle 1
      </Menu.Item>
      <Menu.Item
        onClick={() => {
          setEndselected('Toggle 2');
          setTabSelected('Overview');
        }}
      >
        Toggle 2
      </Menu.Item>
    </Menu>
  );

  return (
    <nav>
      <div className="bets-dash-navbar">
        <h2>{getName(active)}</h2>
        <div className="divider" />
        <div className="members">
          <div className="img-list">
            <img src={profilePic} alt="" />
          </div>
          <div className="add-member">
            <a>
              <FontAwesomeIcon icon={faPlus} /> Add New Team Member
            </a>
          </div>
        </div>
        <Dropdown className="private" trigger={['click']} overlay={menu}>
          <div className="">
            <h5>{endselected} </h5>
            <h5>
              <FontAwesomeIcon icon={faCaretDown} />
            </h5>
          </div>
        </Dropdown>
        <div className="divider ml-1" />
        <div className="notifications">
          <FontAwesomeIcon icon={faBell} />
          <div className="count">0</div>
        </div>
        <div
          className="new-project"
          onClick={() => {
            setEndselected('Toggle 2');
            setTabSelected('Lotteries');
          }}
        >
          <h5>
            <FontAwesomeIcon icon={faPlus} />
            <div className=" my-auto ml-2">New Action</div>
          </h5>
        </div>
      </div>
      <div className="nav-bottom">
        {endselected === 'Toggle 1' ? (
          <div className="tab">
            <div
              className={'tab-itm ' + (tabSelected === 'sub1')}
              onClick={() => setTabSelected('sub1')}
            >
              <h6>Sub Page 1</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub2')}
              onClick={() => setTabSelected('sub2')}
            >
              <h6>Sub Page 2</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub3')}
              onClick={() => setTabSelected('sub3')}
            >
              <h6>Sub Page 3</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub4')}
              onClick={() => setTabSelected('sub4')}
            >
              <h6>Sub Page 4</h6>
            </div>
          </div>
        ) : (
          <div className="tab">
            <div
              className={'tab-itm ' + (tabSelected === 'sub1')}
              onClick={() => setTabSelected('sub1')}
            >
              <h6>Sub Page 4</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub2')}
              onClick={() => setTabSelected('sub2')}
            >
              <h6>Sub Page 5</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub3')}
              onClick={() => setTabSelected('sub3')}
            >
              <h6>Sub Page 6</h6>
            </div>
            <div
              className={'tab-itm ' + (tabSelected === 'sub4')}
              onClick={() => setTabSelected('sub4')}
            >
              <h6>Sub Page 7</h6>
            </div>
          </div>
        )}

        <div className="ml-auto boards active">
          <IconBoards />
          <div className="itm my-auto ml-2">View 1</div>
        </div>
        <div className="divider" />
        <div className="boards">
          <IconList />
          <div className="itm my-auto ml-2">View 2</div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
