import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import Landing from './pages/Landing/Landing';
import MarketPlaceOne from './pages/Home/MarketPlaceOne';
import ManagementOne from './pages/Home/ManagementOne';
import MyAppsOne from './pages/Home/MyAppsOne';
import Marketing from "./pages/Home/Marketing";
import CRM from "./pages/Home/CRM";
import ITndData from "./pages/Home/ITndData";
import Admin from "./pages/Home/Admin";
import CryptoLaw from "./pages/Home/CryptoLaw";
import MyCounsel from "./pages/Home/MyCounsel";
import ProCounsel from "./pages/Home/ProCounsel";
import Regulator from "./pages/Home/Regulator";
import CustomerRelations from "./pages/Home/CustomerRelations";
import Management from "./pages/Home/Managment";
import AddOns from './pages/Home/AddOns';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/landing" component={Landing} />
      <Route exact path="/traction" component={MarketPlaceOne} />
      <Route exact path="/employees" component={ManagementOne} />
      <Route exact path="/myapps1" component={MyAppsOne} />
      <Route exact path="/marketing" component={Marketing}/>
      <Route exact path="/CRM" component={CRM}/>
      <Route exact path = "/ITndData" component = {ITndData}/>
      <Route exact path = "/Admin" component = {Admin}/>
      <Route exact path = "/cryptolaw" component = {CryptoLaw}/>
      <Route exact path = "/mycounsel" component = {MyCounsel}/>
      <Route exact path = "/procounsel" component = {ProCounsel}/>
      <Route exact path = "/regulator" component = {Regulator}/>
      <Route exact path = "/customer-relations" component = {CustomerRelations}/>
      <Route exact path = "/management" component = {Management}/>
      <Route exact path = "/applications" component = {AddOns}/>
    </Switch>
  );
}

export default App;
