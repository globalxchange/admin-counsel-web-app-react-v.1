var RegisterRepo = require('../repository/register.repository')
const request = require('request')
var webhook = 'https://comms.globalxchange.io/gxb/apps/register/user';

exports.getUser = function (req, res) {
    RegisterRepo.find({}, function (err, fetch_all) {
        if (err) {
            res.json({
                success: "false",
                message: "Error while fetching data"
            })
        } else {
            res.json({
                success: "true",
                Total_user: fetch_all.length,
                data: fetch_all
            })
        }
    })
}


exports.addUser = function (req, res) {
    RegisterRepo.findOne({ email: req.body.email }, function (err, fetch) {
      if(err){
          res.json({
              success:"false",
              message:"Unable to fetch users " + err 
          })
      }
      else if(fetch === null){
    request({
        method: 'POST',
        uri: webhook,
        json: {
            email: req.body.email,
            app_code: req.body.app_code,
            userType:req.body.userType
        }
    }, function (err1, result) {
        if (err1) {
            res.json({
                success: "false",
                message: "error" + (err)
            })
        }
        if(result.body.status === true){
            var data = {
                email:req.body.email,
                profile_id:result.body.profile_id,
                app_code:req.body.app_code,
                userType:req.body.userType
            }
            RegisterRepo.create(data,function(error,result1) {
                if(error){
                    res.json({
                        success:"false",
                        message:"error " + error
                    })
                }else{
                    res.json({
                        success:"true",
                        message:result1
                    })
                }
            })
        }
        else{
            res.json({
                success:"false",
                message:result.body.message
            })
        }
    })
}
else{
    var updateuser = {
        email:req.body.email,
        app_code:req.body.app_code,
        userType:req.body.userType
    }
    RegisterRepo.findOneAndUpdate({email:req.body.email},updateuser,function(error,updateresult){
           if(error){
               res.json({
                   success:"false",
                   message:"Error while fetching " + err
               })
           }else{
               res.json({
                   success:"true",
                   data:updateresult
               })
           }
    })
    // res.json({
    //     success:"true",
    //     message:fetch
    // })
}
})
}

exports.AddCorporateProfile = function (req,res) {
    RegisterRepo.findOne({profile_id:req.body.profile_id},function (err,data) {
        if(err){
            res.json({
                success:"false",
                message:"error while fetching users " + err
            })
        }else if(data !== null){    
        for(let i = 0;i<req.body.CorporateProfile.length;i++){
            RegisterRepo.findOneAndUpdate({profile_id:req.body.profile_id},{$addToSet:{CorporateProfile:{$each:[req.body.CorporateProfile[i]]}}},function (error,result) {
                if(error){
                    res.json({
                        success:"false",
                        message:"error while creating corporate profile "+ error
                    })
                }else{
                   return res.json({
                        success:"true",
                        message:"Corporate Profile Created",
                        data
                    })
                }
            })
        }
        }else{
            res.json({
                success:"false",
                message:"user not registered"
            })
        }
    })
}

exports.AddLegalFirm = function (req,res) {
    RegisterRepo.findOne({profile_id:req.body.profile_id},function (err,data) {
        if(err){
            res.json({
                success:"false",
                message:"error while fetching users " + err
            })
        }else if(data!==null){
            if(data.userType === "Accounting Professional"||data.userType === "Admin"){
                for(let i = 0;i<req.body.LegalFirm.length;i++){
           RegisterRepo.findOneAndUpdate({profile_id:req.body.profile_id},{$addToSet:{LegalFirm:{$each:[req.body.LegalFirm[i]]}}},function(error,result){
               if(error){
                   res.json({
                       success:"false",
                       message:"Error while adding Legal firm " + error
                   })
               }else{
                   res.json({
                       success:"true",
                       message: "Legal Firm Added"
                   })
               }
           })
        }
        }else{
            res.json({
                success:"false",
                message:"User doesn't belong to Legal Firm"
            })
        }
        }else{
            res.json({
                success:"false",
                message:"User not registered"
            })
        }
    })
    }
    exports.addRegulator = function(req,res){
        RegisterRepo.findById({_id:req.body._id},function(err,fetch){
            if(err){
                res.json({
                    success:"false",
                    message:"Error occured while fetching data " + err
                })
            }
            if(fetch!==null && fetch.userType === "Admin"){
                var addRegulator = {
                    userType:"Regulator"
                }
                RegisterRepo.findOne({profile_id:req.body.profile_id},function(error1,fetchresult){
                    if(error1){
                        res.json({
                            success:"false",
                            message:"Error while fetching data " + error1
                        })
                    }else if(fetchresult!==null){

                RegisterRepo.findOneAndUpdate({profile_id:req.body.profile_id},addRegulator,function(error,data){
                    if(err){
                        res.json({
                            success:"false",
                            message:"Error while updating data "+ error
                        })
                    }else{
                        res.json({
                            success:"true",
                            message:"User has given authority to be Regulator"
                        })
                    }
                })
            }
            else{
                res.json({
                    success:"false",
                    message:"User with the Counsel ID doesn't exist"
                })
            }
            })
        }else{
                res.json({
                    success:"false",
                    message:"Only Admin has the permission to give Authority"
                })
            }
        })
    }